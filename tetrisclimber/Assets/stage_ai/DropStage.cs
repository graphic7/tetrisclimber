﻿using UnityEngine;
using System.Collections;

public class DropStage : MonoBehaviour {
    private DropManager dropMn;
    private int dir;
    GameObject block;
	// Use this for initialization
	void Start () {
        dropMn = GetComponent<DropManager>();
        dir = 0;
        block = GameObject.Find("block");
    }
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKey(KeyCode.UpArrow))
        {
            if (dir == 0)
                dir++;
            else
                dir = 0;
        }

        if (Input.GetKey(KeyCode.O))
        {
            dropMn.drop(0, dir);
        }
        if (Input.GetKey(KeyCode.J))
        {
            dropMn.drop(1, dir);
        }
        if (Input.GetKey(KeyCode.L))
        {
            dropMn.drop(2, dir);
        }
        if (Input.GetKey(KeyCode.S))
        {
            dropMn.drop(3, dir);
        }
        if (Input.GetKey(KeyCode.Z))
        {
            dropMn.drop(4, dir);
        }
        if (Input.GetKey(KeyCode.I))
        {
            dropMn.drop(5, dir);
        }

    }
}
