﻿using UnityEngine;
using System.Collections;

public class CameraScroll : MonoBehaviour {

	public Camera camera;
	public Camera camera2;
	public Camera camera3;

	private int camera_rotation;
	private bool input_enable;
	private GameObject camera_position;

	private int camera_moved;


	public void CameraView() {
		camera.enabled = true;
		camera2.enabled = false;
		camera3.enabled = false;
	}

	public void Camera2View() {
		camera.enabled = false;
		camera2.enabled = true;
		camera3.enabled = false;
	}

	public void Camera3View() {
		camera.enabled = false;
		camera2.enabled = false;
		camera3.enabled = true;
	}

	// Use this for initialization
	void Start () {
		///camera_rotation = 0, 1, 2, 3
		CameraView();
		input_enable = true;
		camera_rotation = 0;
		camera_moved = 0;
		camera_position = GameObject.Find("Camera_position");
		camera_position.transform.position = new Vector3 (8, 0, 4);
	}

	private void input_delay()
	{
		//3.5秒後に実行する
		StartCoroutine(DelayMethod(1.0f));

	}

	/// <summary>
	/// 渡された処理を指定時間後に実行する
	/// </summary>
	/// <param name="waitTime">遅延時間[ミリ秒]</param>
	/// <param name="action">実行したい処理</param>
	/// <returns></returns>
	private IEnumerator DelayMethod(float waitTime)
	{
		if (input_enable == true) {
			input_enable = false;
			yield return new WaitForSeconds(waitTime);

			if (camera_rotation >= 2) {
				camera_rotation = 0;
				CameraView();
			} else {
				camera_rotation++;
				if(camera_rotation == 1)
					Camera2View();
				else if(camera_rotation == 2)
					Camera3View();
			}
			camera_moved++;
			input_enable = true;
		}
	}

	// Update is called once per frame
	void Update () {
		if(Input.GetKey(KeyCode.LeftControl))
			input_delay();
	}

	public int get_camera_rotation(){
		return camera_rotation;
	}

	public int get_camera_moved(){
		int move_point = camera_moved;
		camera_moved = 0;
		return (move_point % 4);
	}
}
