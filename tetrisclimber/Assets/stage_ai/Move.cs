﻿using UnityEngine;
using System.Collections;

public class Move : MonoBehaviour
{
    public Transform NowObject;
    private Animator animator;
    private CharacterController cc;
    private bool isClimbable = true;
    float moveSpeed = 1f;
    float rotateSpeed = 20f;
    float jumpSpeed = 10f;
    const float walkingDelay = 0.1f;
    const float jumpHightRestriction = 1.5f;
    const float jumpDistance = 0.5f;
    private Transform jumpTarget;
    private Vector3 jumpTargetHitPoint;
    private Vector3 direction = Vector3.zero;
    private float lastWalkTime;
    private Vector3 gravity;
    private SmoothFollow smooth;

    void Awake()
    {
        cc = GetComponent<CharacterController>();
        animator = GetComponent<Animator>();
        smooth = Camera.main.GetComponent<SmoothFollow>();

    }

    void Update()
    {
        if (animator.GetCurrentAnimatorStateInfo(0).IsName("Idle_ToJumpUpHigh"))
        {
            animator.SetBool("jumpUpHigh", false);
            //Vector3 grabPosition = jumpTarget.position + new Vector3(0, jumpTarget.localScale.y / 2, -jumpTarget.localScale.z / 2 - 0.1f);
            Vector3 grabPosition = jumpTargetHitPoint;
            grabPosition.y = jumpTarget.transform.position.y + jumpTarget.localScale.y / 2;
            animator.MatchTarget(grabPosition, jumpTarget.rotation, AvatarTarget.RightHand, new MatchTargetWeightMask(Vector3.one, 1f), animator.GetFloat("MatchStart"), animator.GetFloat("MatchEnd"));
            cc.enabled = false;
            Debug.Log(grabPosition);
            return;
        }
        if (!cc.isGrounded)
        {
            direction = Vector3.zero;
            gravity += Physics.gravity * Time.deltaTime;
            //cc.Move(gravity);
            //Debug.Log("On Air gravity" + gravity);
            return;
        }
        gravity = Vector3.zero;

        Vector3 inputVector = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
        if (inputVector != Vector3.zero)
        {
            direction = inputVector.normalized;
            direction = Quaternion.AngleAxis(smooth.rotation, Vector3.up) * direction;
            Quaternion rot = Quaternion.LookRotation(direction);
            transform.rotation = Quaternion.Slerp(transform.rotation, rot, rotateSpeed * Time.deltaTime);
            animator.SetBool("walk", true);
            lastWalkTime = Time.time;
            cc.enabled = true;

            NowObject.position = transform.position;
        }
        else
        {
            if (Time.time - lastWalkTime > walkingDelay)
            {
                animator.SetBool("walk", false);
            }
        }

        if (Input.GetButton("Jump"))
        {
            //direction.y = jumpSpeed;
            if (isClimbable)
            {
                animator.SetBool("jumpUpHigh", true);
            }
            else
            {
                animator.SetBool("jumping", true);
            }
        }
        //cc.Move(direction * moveSpeed * Time.deltaTime);
    }

    // FixedUpdate is called once per physics loop
    void FixedUpdate()
    {
        if (direction != Vector3.zero)
        {
            //cc.SimpleMove(direction * moveSpeed);
        }

        // nowの位置を変える
        NowObject.transform.position = Vector3floor(transform.position);

        isClimbable = false;
        RaycastHit hit;
        //if (Physics.Raycast(transform.position + Vector3.up * transform.localScale.y, direction, out hit, jumpDistance))
        if (Physics.Raycast(transform.position, direction, out hit, jumpDistance))
        {
            if (hit.transform.tag == "tetrisBlock" && hit.collider.bounds.size.y < jumpHightRestriction)
            {
                // 1. 올라가려는 것이 올라갈수 있는 최대 높이 보다 작은가?
                // 2. 올라갔을때 서 있을 수 있는 충분한 공간이 존재하는가?
                if (!Physics.Raycast(hit.transform.position, Vector3.up, cc.bounds.size.y + hit.collider.bounds.size.y / 2))
                {
                    isClimbable = true;
                    jumpTarget = hit.transform;
                    jumpTargetHitPoint = hit.point;
                    Debug.Log("hit " + jumpTargetHitPoint);


                    // TODO : 물체의 끝부분(경계)에서 오르기를 했을경우, 대각선일떄의 처리도 있어야됨.
                    // 현재위치의 상하좌우 이외인가? = 대각선
                    int[] pos = Vector3ToIntArr(hit.transform.position);
                    int[] pos2 = Vector3ToIntArr(transform.position);
                    int diff = 0;
                    for (int i = 0; i < 3; i++)
                    {
                        diff += Mathf.Abs(pos[i] - pos2[i]);
                    }
                    Debug.Log(diff);
                    if (diff == 2)
                    {

                    }
                }
            }
        }
    }

    int[] Vector3ToIntArr(Vector3 vec)
    {
        int[] pos = new int[3];
        pos[0] = (int)vec.x;
        pos[1] = (int)vec.y;
        pos[2] = (int)vec.z;
        return pos;
    }

    Vector3 Vector3floor(Vector3 vec)
    {
        int[] pos = Vector3ToIntArr(vec);
        return new Vector3(pos[0], pos[1], pos[2]);
    }
}