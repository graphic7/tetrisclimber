﻿using UnityEngine;
using System.Collections;

public class BlockInsManager : MonoBehaviour {
	private Drop_Manager dropMn;
	private GameObject stand_block;
	private int low_size;
	public GameObject block_instance;
	// Use this for initialization
	void Start () {
		stand_block = GameObject.Find("Now");
		dropMn = stand_block.GetComponent<Drop_Manager>();
	}

	// Update is called once per frame
	/*
	 * Drop_Managerのmap管理では最大値を0として、他の地点の大きさとの差で様々な計算を行っている。
	 * しかし、ブロック生成は座標指定で行っているので、map最小値の地点の実質的な大きさが0でないと
	 * ブロック生成の際に位置がずれてしまう。
	 * これを防ぐために、生成したブロックの集合Block_ins自体の位置を調節する。
	 * この際、明らかに必要なくなった(今回では座標が-5以下)子のブロックを消す。
	 */
	void Update () {
		if (dropMn.get_sizeAlert() != 0) {
			int alert = dropMn.get_sizeAlert();
			block_instance.transform.position = new Vector3 (block_instance.transform.position.x, block_instance.transform.position.y - alert, block_instance.transform.position.z);
			foreach (Transform n in block_instance.transform) {
				if (n.gameObject.transform.position.y < - 5)
					GameObject.Destroy (n.gameObject);
				/*
				 * 子のブロックを更に子単位で消す方法。上手くいかなかった。
				foreach (Transform m in n.gameObject.transform) {
					if (m.gameObject.transform.childCount > 0) {
						foreach (Transform l in m.gameObject.transform) {
							if(m.gameObject.transform.position.y < 0)
								GameObject.Destroy (l.gameObject);
							if(m.gameObject.transform.childCount == 0)
								GameObject.Destroy (m.gameObject);
						}
					}
					else if(m.gameObject.transform.position.y < 0)
						GameObject.Destroy (m.gameObject);
				}
				if (n.gameObject.transform.childCount == 0)
					GameObject.Destroy (n.gameObject);
				*/
			}
			dropMn.sizeAlert_reset ();
		}
	}
}
