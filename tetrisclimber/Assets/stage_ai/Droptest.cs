﻿using UnityEngine;
using System;
using System.Collections;

public class Droptest : MonoBehaviour {
	/*
	 * メインプログラム
	 * Nowの操作を行う。
	 * 
	 * テトリスのブロックは4つの正方形からなる。
	 * 自分の歩いた道のり4マス分の形が毎回ブロックとして生成されるようにする。
	 * その為には斜め移動、バックを許してはいけない。
	 */
	public GameObject block_O;
	public GameObject block_J;
	public GameObject block_L;
	public GameObject block_S;
	public GameObject block_Z;
	public GameObject block_I;
	public GameObject cameraCentral;
	private GameObject foot;
	private GameObject stand_block;
	private int block_size;
	private int stand_xpos;
	private int stand_ypos;
	private const int width = 7;
	private const int height = 7;
	private Drop_Manager dropMn;
	private FootPrintsManager footMn;
	private CameraScroll cameraSc;
	private int stand_dir;//up = 0, right = 1, down = 2, left = 3
	private const int up = 0;
	private const int right = 1;
	private const int down = 2;
	private const int left = 3;
	private int footprints_count;
	private int turn_element;
	public int size;
	public int dir;

	private bool input_enable;

	//現在のカメラは前回nowが移動した時のカメラからどれくらい回転しているか
	private int camera_moved;

	// Use this for initialization
	void Start () {
		camera_moved = 0;
		foot = GameObject.Find("FootPrints");
		stand_block = GameObject.Find("Now");
		block_O.transform.position = new Vector3 (0, -4, 0);
		block_J.transform.position = new Vector3 (0, -4, 0);
		block_L.transform.position = new Vector3 (0, -4, 0);
		block_S.transform.position = new Vector3 (0, -4, 0);
		block_Z.transform.position = new Vector3 (0, -4, 0);
		block_I.transform.position = new Vector3 (0, -5, 0);
		foot.transform.position = new Vector3 (0, -4, 0);
		block_size = 1;
		//Nowの初期座標
		stand_xpos = 3;
		stand_ypos = 3;
		stand_dir = -1; //Nowが向いている方向(-1は何処も向いていない)
		dir = 0;
		stand_block.transform.position = new Vector3 (3, 0, 3);
		dropMn = GetComponent<Drop_Manager>();
		footMn = GetComponent<FootPrintsManager> ();
		cameraSc = cameraCentral.GetComponent<CameraScroll> ();

		input_enable = true;
	}

	//Nowのy座標の立ち位置を調整し、Nowが生成されたブロックの上に立っているようにする。
	private void size_matching(){
		size = dropMn.get_map ((int)stand_block.transform.position.x, (int)stand_block.transform.position.z) - dropMn.get_lowSize();
		if (size != (int)stand_block.transform.position.y)
			stand_block.transform.position = new Vector3(stand_block.transform.position.x, size, stand_block.transform.position.z);
		
	}

	//ブロックを落とすよう命令する。
	private void dropBlock(int blocknum, int standBlock_dir){
		if(standBlock_dir == up || standBlock_dir == down)
			dropMn.drop (blocknum, 0);
		else
			dropMn.drop (blocknum, 1);
	}


	private void input_delay()
	{
		//3.5秒後に実行する
		if (Input.GetKey (KeyCode.UpArrow) && stand_dir != down) {
			StartCoroutine (DelayMethod (0.3f, 0));
		}
		if (Input.GetKey (KeyCode.DownArrow) && stand_dir != up) {
			StartCoroutine (DelayMethod (0.3f, 1));
		}
		if (Input.GetKey (KeyCode.LeftArrow) && stand_dir != right) {
			StartCoroutine (DelayMethod (0.3f, 2));
		}
		if (Input.GetKey (KeyCode.RightArrow) && stand_dir != left) {
			StartCoroutine (DelayMethod (0.3f, 3));
		}
	}

	/// <summary>
	/// 渡された処理を指定時間後に実行する
	/// </summary>
	/// <param name="waitTime">遅延時間[ミリ秒]</param>
	/// <param name="action">実行したい処理</param>
	/// <returns></returns>
	private IEnumerator DelayMethod(float waitTime, int move_dir)
	{
		int camera_rotation = 0;

		if (input_enable == true) {
			input_enable = false;
			yield return new WaitForSeconds(waitTime);

			if (move_dir == 0 && stand_ypos <= height - 2) {
				stand_ypos++;
				stand_block.transform.position = new Vector3 (stand_block.transform.position.x, 0, stand_block.transform.position.z + (float)(block_size));
				size_matching ();
				footprints_count = footMn.put_footprints (stand_block.transform.position.x, stand_block.transform.position.y, stand_block.transform.position.z, up);
				if (footprints_count < 1) {
					dropBlock (-footprints_count, up);
					stand_dir = -1;
				} else
					//現在のカメラから見た方向
					stand_dir = (up + camera_rotation) % 4;
			}

			if (move_dir == 1 && stand_ypos >= 1 || move_dir == 3 && camera_rotation == 1 && stand_ypos >= 1 || move_dir == 0 && camera_rotation == 2 && stand_ypos >= 1 || move_dir == 1 && camera_rotation == 3 && stand_ypos >= 1) {
				stand_ypos--;
				stand_block.transform.position = new Vector3 (stand_block.transform.position.x, 0, stand_block.transform.position.z - (float)(block_size));
				size_matching ();
				footprints_count = footMn.put_footprints (stand_block.transform.position.x, stand_block.transform.position.y, stand_block.transform.position.z, down);
				if (footprints_count < 1) {
					dropBlock (-footprints_count, down);
					stand_dir = -1;
				} else
					stand_dir = (down + camera_rotation) % 4;
			}
			if (move_dir == 2 && camera_rotation == 0 && stand_xpos >= 1 || move_dir == 0 && camera_rotation == 1 && stand_xpos >= 1 || move_dir == 1 && camera_rotation == 2 && stand_xpos >= 1 || move_dir == 2 && camera_rotation == 3 && stand_xpos >= 1) {
				stand_xpos--;
				stand_block.transform.position = new Vector3 (stand_block.transform.position.x - (float)(block_size), 0, stand_block.transform.position.z);
				size_matching ();
				footprints_count = footMn.put_footprints (stand_block.transform.position.x, stand_block.transform.position.y, stand_block.transform.position.z, left);
				if (footprints_count < 1) {
					dropBlock (-footprints_count, left);
					stand_dir = -1;
				} else
					stand_dir = (left + camera_rotation) % 4;
			}
			if (move_dir == 3 && camera_rotation == 0 && stand_xpos <= width - 2 || move_dir == 2 && camera_rotation == 1 && stand_xpos <= width - 2 || move_dir == 3 && camera_rotation == 2 && stand_xpos <= width - 2 || move_dir == 0 && camera_rotation == 3 && stand_xpos <= width - 2) {
				stand_xpos++;
				stand_block.transform.position = new Vector3 (stand_block.transform.position.x + (float)(block_size), 0, stand_block.transform.position.z);
				size_matching ();
				footprints_count = footMn.put_footprints (stand_block.transform.position.x, stand_block.transform.position.y, stand_block.transform.position.z, right);
				if (footprints_count < 1) {
					dropBlock (-footprints_count, right);
					stand_dir = -1;
				} else
					stand_dir = (right + camera_rotation) % 4;
			}
			input_enable = true;
		}
	}

	// Update is called once per frame
	void Update () {
		try{
			size_matching();
			/*
			 * 移動について、Stageから出ないように、更にバックが出来ない(stand_dir = -1でない場合は来た道を戻れない)ようにする。
			 * 
			 * footprints_countにはブロック生成のタイミングであれば(-ブロックの番号)が入っている。それ以外の場合は1が入る。
			 */
			if(input_enable == true)
			input_delay();

			//リセット
			if (Input.GetKey (KeyCode.R)) {
				dropMn.reset();
				footMn.reset();
				block_size = 1;
				stand_xpos = 3;
				stand_ypos = 3;
				stand_dir = -1;
				dir = 0;
				stand_block.transform.position = new Vector3 (3, 0, 3);
			}

			//終了
			if (Input.GetKey (KeyCode.Escape)) {
				Application.Quit();
			}
				
		}
		catch(Exception e){
			Console.WriteLine (e);
		}

	}
}
