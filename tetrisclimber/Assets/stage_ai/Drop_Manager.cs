﻿using UnityEngine;
using System;

public class Drop_Manager : MonoBehaviour {
    private int[,] stage;
	private DropBlock_Processing drop_pro;
    private const int width = 7;
    private const int height = 7;
    private int low_size;
    // Use this for initialization
    private int[] low_pos;
    private int[] low_point;
	private int lowsize_alert;
	private int lowsize_alert_alert;
    private GameObject player;

	/*
	 * drop_point...点数計算
	 * drop_action...実際にmapに書き込む
	 * drop...二つのメソッドの実行、ブロックの投下
	 */

	/*
	* ブロックの落下位置の決定、ブロックの形の決定、DropBlock_Processingに直に命令を出す役割。
	* ブロックの位置はint二重配列map管理で行うが、最大値を0で固定する。
	*/
    void Awake()
    {
        stage = new int[width, height];
        for (int i = 0; i < width; i++)
            for (int j = 0; j < height; j++)
                stage[i, j] = 0;
        player = GameObject.Find("Now");
		drop_pro = GetComponent<DropBlock_Processing>();
        low_size = 0;
		lowsize_alert = 0;
		lowsize_alert_alert = 0;
    }

    void Start () {
    }

	//リセット
	//DropBlock_Processingにもリセット命令を送る。
	public void reset(){
		for (int i = 0; i < width; i++)
			for (int j = 0; j < height; j++)
				stage[i, j] = 0;
		low_size = 0;
		lowsize_alert = 0;
		drop_pro.reset ();
	}

	//指定の位置、ブロックの点数計算。
    private int drop_point(int x, int y, int blocknum, int block_dir)
    {
        switch (blocknum)
        {
            //o
            //現地点と隣の地点のブロックの高さが同じであれば0、それ以外は10
            //stageの端(番号width, height側)であれば、確認せず0。
            case 0:
                if (block_dir == 1 && y < height - 1 && stage[x, y + 1] == stage[x, y] || block_dir == 0 && x < width - 1 && stage[x + 1, y] == stage[x, y])
                    return 0;
                else if (block_dir == 1 && y < height - 1 || block_dir == 0 && x < width - 1)
                    return 4;
                else
                    return 50;
            //J
		case 1:
                //現地点が隣の地点より2以上低かった場合、Jを逆さまにした形で現地点に落とす。きっちり収まった場合0、空洞が出来てしまった場合3
                //stageの端(番号width, height側)であれば、確認せず。
			if (block_dir == 1 && y < height - 1 && stage [x, y + 1] - stage [x, y] >= 2 || block_dir == 0 && x < width - 1 && stage [x + 1, y] - stage [x, y] >= 2) {
				if (block_dir == 1 && stage [x, y + 1] - stage [x, y] == 2 || block_dir == 0 && stage [x + 1, y] - stage [x, y] == 2)
					return 0;
				else
					return 3;
			}

                //現地点から隣の地点、更に隣の地点を参照し、一つ先の地点より二つ先の地点の高さが1小さく、現地点が一つ先の地点以下の場合にJの左側を下にして落とす。現地点と一つ先の地点の高さが同じであれば0、そうでなければ3
                //現地点がstageの端(番号width, height側)、またはその隣であれば、確認せず。
                else if (block_dir == 1 && y < height - 2 && stage [x, y + 1] == stage [x, y + 2] + 1 && stage [x, y + 1] >= stage [x, y] || block_dir == 0 && x < width - 2 && stage [x + 1, y] == stage [x + 2, y] + 1 && stage [x + 1, y] >= stage [x, y]) {
				if (block_dir == 1 && stage [x, y] == stage [x, y + 1] || block_dir == 0 && stage [x, y] == stage [x + 1, y])
					return 0;
				else
					return 3;
			}

                //現地点が隣の地点とさらに隣の地点いずれか同士が同じ高さで、その二つ以外の地点が二つ以下の高さであった場合に,Jの右側を下にして落とす。三つが同じ高さであった場合は0、二つのみの場合は4
                //現地点がstageの端(番号width, height側)、またはその隣であれば、確認せず。
                else if (block_dir == 1 && y < height - 2 && (stage [x, y + 1] == stage [x, y] && stage [x, y + 2] <= stage [x, y] || stage [x, y + 2] == stage [x, y] && stage [x, y + 1] <= stage [x, y] || stage [x, y + 1] == stage [x, y + 2] && stage [x, y] <= stage [x, y + 1]) ||
			                      block_dir == 0 && x < width - 2 && (stage [x + 1, y] == stage [x, y] && stage [x + 2, y] <= stage [x, y] || stage [x + 2, y] == stage [x, y] && stage [x + 1, y] <= stage [x, y] || stage [x + 1, y] == stage [x + 2, y] && stage [x, y] <= stage [x + 1, y])) {
				if (block_dir == 1 && stage [x, y + 1] == stage [x, y] && stage [x, y + 2] == stage [x, y] && stage [x, y + 1] == stage [x, y + 2] || block_dir == 0 && stage [x + 1, y] == stage [x, y] && stage [x + 2, y] == stage [x, y] && stage [x + 1, y] == stage [x + 2, y])
					return 0;
				else
					return 4;
			}
                //現地点が隣の地点と同じ高さであった場合,Jをそのまま落とす。ポイントは5
			else if (block_dir == 1 && y < height - 1 && stage [x, y + 1] == stage [x, y] || block_dir == 0 && x < width - 1 && stage [x + 1, y] == stage [x, y])
				return 5;
			else {
				return 50;
			}
            //L
            case 2:
                //現地点が隣の地点より2以上高かった場合、Lを逆さまにした形で現地点に落とす。きっちり収まった場合0、空洞が出来てしまった場合3
                //stageの端(番号width, height側)であれば、確認せず。
                if (block_dir == 1 && y < height - 1 && stage[x, y] - stage[x, y + 1] >= 2 || block_dir == 0 && x < width - 1 && stage[x, y] - stage[x + 1, y] >= 2)
                {
                    if (block_dir == 1 && stage[x, y] - stage[x, y + 1] == 2 || block_dir == 0 && stage[x, y] - stage[x + 1, y] == 2)
                        return 0;
                    else
                        return 3;
                }

                //現地点から隣の地点、更に隣の地点を参照し、一つ先の地点より現地点の高さが1小さく、二つ先の地点が一つ先の地点以下の場合にLの右側を下にして落とす。二つ先の地点と一つ先の地点の高さが同じであれば0、そうでなければ3
                //現地点がstageの端(番号width, height側)、またはその隣であれば、確認せず。
                else if (block_dir == 1 && y < height - 2 && stage[x, y] == stage[x, y + 1] + 1 && stage[x, y + 1] >= stage[x, y + 2] || block_dir == 0 && x < width - 2 && stage[x, y] == stage[x + 1, y] + 1 && stage[x + 1, y] >= stage[x + 2, y])
                {
                    if (block_dir == 1 && stage[x, y + 2] == stage[x, y + 1] || block_dir == 0 && stage[x + 2, y] == stage[x + 1, y])
                        return 0;
                    else
                        return 3;
                }

                //現地点が隣の地点とさらに隣の地点いずれか同士が同じ高さで、その二つ以外の地点が二つの高さ以下であった場合に,Lの左側を下にして落とす。三つが同じ高さであった場合は0、二つのみの場合は4
                //現地点がstageの端(番号width, height側)、またはその隣であれば、確認せず。
                else if (block_dir == 1 && y < height - 2 && (stage[x, y + 1] == stage[x, y] && stage[x, y + 2] <= stage[x, y] || stage[x, y + 2] == stage[x, y] && stage[x, y + 1] <= stage[x, y] || stage[x, y + 1] == stage[x, y + 2] && stage[x, y] <= stage[x, y + 1]) || 
                    block_dir == 0 && x < width - 2 && (stage[x + 1, y] == stage[x, y] && stage[x + 2, y] <= stage[x, y] || stage[x + 2, y] == stage[x, y] && stage[x + 1, y] <= stage[x, y] || stage[x + 1, y] == stage[x + 2, y] && stage[x, y] <= stage[x + 1, y]))
                {
                    if (block_dir == 1 && stage[x, y + 1] == stage[x, y] && stage[x, y + 2] == stage[x, y] && stage[x, y + 1] == stage[x, y + 2] || block_dir == 0 && stage[x + 1, y] == stage[x, y] && stage[x + 2, y] == stage[x, y] && stage[x + 1, y] == stage[x + 2, y])
                        return 0;
                    else
                        return 4;
                }
                //現地点が隣の地点と同じ高さであった場合,Lをそのまま落とす。ポイントは5
                else if (block_dir == 1 && y < height - 1 && stage[x, y + 1] == stage[x, y] || block_dir == 0 && x < width - 1 && stage[x + 1, y] == stage[x, y])
                    return 5;
                else
                    return 50;
            //s
		case 3:
                //現地点から一つ先、二つ先の地点を見て、一つ先より二つ先が1高く、現地点が一つ先以下の高さの場合sのまま落とす。現地点と一つ先の高さが同じであれば0、そうでなければ2
                //現地点がstageの端(番号width, height側)、またはその隣であれば、確認せず。
			if (block_dir == 1 && y < height - 2 && stage [x, y + 2] == stage [x, y + 1] + 1 && stage [x, y + 1] >= stage [x, y] || block_dir == 0 && x < width - 2 && stage [x + 2, y] == stage [x + 1, y] + 1 && stage [x + 1, y] >= stage [x, y]) {
				return 0;
			}
                //現地点から一つ先を見て、現地点が一つ先より1高ければ、sを90度回転させた形で落とす。ポイントは1
                //現地点がstageの端(番号width, height側)であれば、確認せず。
                else if (block_dir == 1 && y < height - 1 && stage [x, y] == stage [x, y + 1] + 1 || block_dir == 0 && x < width - 1 && stage [x, y] == stage [x + 1, y] + 1)
				return 1;
			else if (block_dir == 1 && y < height - 1 || block_dir == 0 && x < width - 1)
				return 5;
			else
				return 50;
            //z
            case 4:
                //現地点から一つ先、二つ先の地点を見て、一つ先より現地点が1高く、二つ先が一つ先以下の高さの場合zのまま落とす。二つ先と一つ先の高さが同じであれば0、そうでなければ2
                //現地点がstageの端(番号width, height側)、またはその隣であれば、確認せず。
                if (block_dir == 1 && y < height - 2 && stage[x, y] == stage[x, y + 1] + 1 && stage[x, y + 1] >= stage[x, y + 2] || block_dir == 0 && x < width - 2 && stage[x, y] == stage[x + 1, y] + 1 && stage[x + 1, y] >= stage[x + 2, y])
                {
                    return 0;
                }
                //現地点から一つ先を見て、一つ先が現地点より1高ければ、zを90度回転させた形で落とす。ポイントは1
                //現地点がstageの端(番号width, height側)であれば、確認せず。
                else if (block_dir == 1 && y < height - 1 && stage[x, y + 1] == stage[x, y] + 1 || block_dir == 0 && x < width - 1 && stage[x + 1, y] == stage[x, y] + 1)
                    return 1;
				else if (block_dir == 1 && y < height - 1 || block_dir == 0 && x < width - 1)
					return 5;
                else
                    return 50;
            //I
		case 5:
                //現地点より四つ先まで見て、三つ同じ高さ、残る一つはそれら以下の場合、Iを横にして落とす。全て同じ高さなら0、それ以外は2
			if (block_dir == 1 && y < height - 3) {

				if (stage[x, y] == stage[x, y + 1]) {
					if (stage [x, y] == stage [x, y + 2]) {
						if (stage [x, y] == stage [x, y + 3])
							return 0;
						else if (stage [x, y] >= stage [x, y + 2])
							return 2;
					} else if (stage [x, y] == stage [x, y + 3] && stage [x, y] >= stage [x, y + 2])
						return 2;
				}
				else if (stage [x, y + 2] == stage [x, y + 3] && stage[x, y + 2] >= stage[x, y] && stage[x, y + 2] >= stage[x, y + 1] && (stage[x, y + 2] == stage[x, y] || stage[x, y + 2] == stage[x, y + 1])) {
						return 2;
				}
			}
			else if (block_dir == 0 && x < width - 3) {
				
				if (stage [x, y] == stage [x + 1, y]) {
					if (stage [x, y] == stage [x + 2, y]) {
						if (stage [x, y] == stage [x + 3, y])
							return 0;
						else if(stage[x, y] >= stage[x + 2, y])
							return 2;
					} else if (stage [x, y] == stage [x + 3, y] && stage [x, y] >= stage [x + 2, y])
						return 2;
				} else if (stage [x + 2, y] == stage [x + 3, y] && stage[x + 2, y] >= stage[x, y] && stage[x + 2, y] >= stage[x + 1, y] && (stage[x + 2, y] == stage[x, y] || stage[x + 2, y] == stage[x + 1, y])) {
						return 2;
				}
			}
                //ポイントは現地点の深さに4を足した値
			return 4 + stage[x, y];

			break;
            default:
                return 10;
        }
		return 100;
    }

	//関数drop_actionでマップ更新を行った際、最大値が0を越えてしまった時に引数のexceedで全体を引く。
	//また、BlockInsManagerで使う「map最小値の地点の実質的な大きさが0でない」時に現在の最小値の実質的な大きさ
	//を持つ変数lowsize_alertを決定する場所でもある。関数drop_action中必ず呼ばれる。
	private void exceed_processing(int exceed){
		int lowSize_now = 0;
		if (exceed > 0) {
			for (int i = 0; i < width; i++) {
				for (int j = 0; j < height; j++) {
					stage [i, j] -= exceed;
					if (stage [i, j] < lowSize_now)
						lowSize_now = stage [i, j];
				}
			}
			if (low_size - exceed < lowSize_now) {
				lowsize_alert = lowSize_now - (low_size - exceed);
			}
		} else {
			for (int i = 0; i < width; i++) {
				for (int j = 0; j < width; j++) {
					if (stage [i, j] < lowSize_now)
						lowSize_now = stage [i, j];
				}
			}
			if (low_size < lowSize_now)
				lowsize_alert = lowSize_now - low_size;
		}
		low_size = lowSize_now;
	}

    /*
     * mapの更新,blockの回転の有無(時計回りに0, 1, 2, 3を返す)
     * 採点の際に使ったheight, widthの端に対する条件に違反している座標が渡された場合のエラー処理は呼び出し元で行う。
     * 最高値が0を越えた場合、変数exceedで処理する(具体的には、exceedに最高値の値を入れ、map全体をexceedで引く)。
     * exceedにはブロックが追加された直後の高さが入っており、もし前の最大値を超えていれば、0以上の数が入っているはずである。
     */
    private int drop_action(int x, int y, int blocknum, int block_dir)
    {
        int exceed = 0;
        switch (blocknum)
        {
            //o
            //oブロックを配置。
		case 0:
			if (block_dir == 1) {
				if (stage [x, y] >= stage [x, y + 1]) {
					stage [x, y] += 2;
					stage [x, y + 1] = stage [x, y];
				} else {
					stage [x, y + 1] += 2;
					stage [x, y] = stage [x, y + 1];
				}
				if (stage [x, y] > 0)
					exceed = stage [x, y];
				exceed_processing (exceed);
				return 0;
			} else {
				if (stage [x, y] >= stage [x + 1, y]) {
					stage [x, y] += 2;
					stage [x + 1, y] = stage [x, y];
				} else {
					stage [x + 1, y] += 2;
					stage [x, y] = stage [x + 1, y];
				}
				if (stage [x, y] > 0)
					exceed = stage [x, y];
				exceed_processing (exceed);
				return 0;
			}
            //J
            case 1:

                if (block_dir == 1)
                {
                    //現地点が隣の地点より2以上低かった場合、Jを逆さまにした形で現地点に落とす。
                    if (y < height - 1 && stage[x, y + 1] - stage[x, y] >= 2)
                    {
                        stage[x, y + 1]++;
                        stage[x, y] = stage[x, y + 1];
                        if (stage[x, y] > 0)
                            exceed = stage[x, y];
					exceed_processing (exceed);
                        return 2;
                    }

                    //現地点から隣の地点、更に隣の地点を参照し、一つ先の地点より二つ先の地点の高さが1小さく、現地点が一つ先の地点以下の場合にJの左側を下にして落とす。
                    else if (y < height - 2 && stage[x, y + 1] == stage[x, y + 2] + 1 && stage[x, y + 1] >= stage[x, y])
                    {
                        stage[x, y + 1]++;
                        stage[x, y] = stage[x, y + 1];
                        stage[x, y + 2] = stage[x, y];
                        if (stage[x, y] > 0)
                            exceed = stage[x, y];
					exceed_processing (exceed);
                        return 3;
                    }

                    //現地点が隣の地点とさらに隣の地点いずれかが同じ高さであった場合に,Jの右側を下にして落とす。
                    else if (y < height - 2 && (stage[x, y + 1] == stage[x, y] && stage[x, y + 2] <= stage[x, y] || stage[x, y + 2] == stage[x, y] && stage[x, y + 1] <= stage[x, y] || stage[x, y + 1] == stage[x, y + 2] && stage[x, y] <= stage[x, y + 1]))
                    {
                        if(stage[x, y] == stage[x, y + 1])
                        {
                            stage[x, y]+= 2;
                            stage[x, y + 1]++;
                            stage[x, y + 2] = stage[x, y + 1];
                        }
                        else if(stage[x, y] == stage[x, y + 2])
                        {
                            stage[x, y]+= 2;
                            stage[x, y + 2]++;
                            stage[x, y + 1] = stage[x, y + 2];
                        }
                        else
                        {
                            stage[x, y + 1]++;
                            stage[x, y + 2]++;
                            stage[x, y] = stage[x, y + 1] + 1;
                        }

                        if (stage[x, y] > 0)
                            exceed = stage[x, y];
					exceed_processing (exceed);
                        return 1;
                    }
                    //以上どれにも当てはまらない場合は、Jのまま落とす。
                    else
                    {
                        if(stage[x, y] >= stage[x, y + 1])
                        {
                            stage[x, y]++;
                            stage[x, y + 1] = stage[x, y] + 2;
                        }
                        else
                        {
                            stage[x, y + 1]+= 3;
                            stage[x, y] = stage[x, y + 1] - 2;
                        }
                        if (stage[x + 1, y] > 0)
                            exceed = stage[x + 1, y];
					exceed_processing (exceed);
                        return 0;
                    }
                }
                //block_dir == 0の場合(J)
                else
                {
                    //現地点が隣の地点より2以上低かった場合、Jを逆さまにした形で現地点に落とす。
                    if (x < width - 1 && stage[x + 1, y] - stage[x, y] >= 2)
                    {
                        stage[x + 1, y]++;
                        stage[x, y] = stage[x + 1, y];
                        if (stage[x, y] > 0)
                            exceed = stage[x, y];
					exceed_processing (exceed);
                        return 2;
                    }

                    //現地点から隣の地点、更に隣の地点を参照し、一つ先の地点より二つ先の地点の高さが1小さく、現地点が一つ先の地点以下の場合にJの左側を下にして落とす。
                    else if (x < width - 2 && stage[x + 1, y] == stage[x + 2, y] + 1 && stage[x + 1, y] >= stage[x, y])
                    {
                        stage[x + 1, y]++;
                        stage[x, y] = stage[x + 1, y];
                        stage[x + 2, y] = stage[x, y];
                        if (stage[x, y] > 0)
                            exceed = stage[x, y];
					exceed_processing (exceed);
                        return 3;
                    }

                    //現地点が隣の地点とさらに隣の地点いずれかが同じ高さであった場合に,Jの右側を下にして落とす。
                    else if (x < width - 2 && (stage[x + 1, y] == stage[x, y] && stage[x + 2, y] <= stage[x, y] || stage[x + 2, y] == stage[x, y] && stage[x + 1, y] <= stage[x, y] || stage[x + 1, y] == stage[x + 2, y] && stage[x, y] <= stage[x + 1, y]))
                    {
                        if (stage[x, y] == stage[x + 1, y])
                        {
                            stage[x, y] += 2;
                            stage[x + 1, y]++;
                            stage[x + 2, y] = stage[x + 1, y];
                        }
                        else if (stage[x, y] == stage[x + 2, y])
                        {
                            stage[x, y] += 2;
                            stage[x + 2, y]++;
                            stage[x + 1, y] = stage[x + 2, y];
                        }
                        else
                        {
                            stage[x + 1, y]++;
                            stage[x + 2, y]++;
                            stage[x, y] = stage[x + 1, y] + 1;
                        }

                        if (stage[x, y] > 0)
                            exceed = stage[x, y];
					exceed_processing (exceed);
                        return 1;
                    }
                    //以上どれにも当てはまらない場合は、Jのまま落とす。
                    else
                    {
                        if (stage[x, y] >= stage[x + 1, y])
                        {
                            stage[x, y]++;
                            stage[x + 1, y] = stage[x, y] + 2;
                        }
                        else
                        {
                            stage[x + 1, y] += 3;
                            stage[x, y] = stage[x + 1, y] - 2;
                        }
                        if (stage[x + 1, y] > 0)
                            exceed = stage[x + 1, y];
					exceed_processing (exceed);
                        return 0;
                    }
                }
            //L
            case 2:
                if (block_dir == 1)
                {
                    //現地点が隣の地点より2以上高かった場合、Lを逆さまにした形で現地点に落とす。
                    if (y < height - 1 && stage[x, y] - stage[x, y + 1] >= 2)
                    {
                        stage[x, y]++;
                        stage[x, y + 1] = stage[x, y];
                        if (stage[x, y] > 0)
                            exceed = stage[x, y];
					exceed_processing (exceed);
                        return 2;
                    }

                    //現地点から隣の地点、更に隣の地点を参照し、一つ先の地点より現地点の高さが1小さく、二つ先が一つ先の地点以下の場合にLの右側を下にして落とす。
                    else if (y < height - 2 && stage[x, y + 1] == stage[x, y] + 1 && stage[x, y + 1] >= stage[x, y + 2])
                    {
                        stage[x, y + 1]++;
                        stage[x, y] = stage[x, y + 1];
                        stage[x, y + 2] = stage[x, y];
                        if (stage[x, y] > 0)
                            exceed = stage[x, y];
					exceed_processing (exceed);
                        return 1;
                    }

                    //現地点が隣の地点とさらに隣の地点いずれかが同じ高さであった場合に,Lの左側を下にして落とす。
                    else if (y < height - 2 && (stage[x, y + 1] == stage[x, y] && stage[x, y + 2] <= stage[x, y] || stage[x, y + 2] == stage[x, y] && stage[x, y + 1] <= stage[x, y] || stage[x, y + 1] == stage[x, y + 2] && stage[x, y] <= stage[x, y + 1]))
                    {
                        if (stage[x, y] == stage[x, y + 1])
                        {
                            stage[x, y]++;
                            stage[x, y + 1]++;
                            stage[x, y + 2] = stage[x, y] + 1;
                        }
                        else if (stage[x, y] == stage[x, y + 2])
                        {
                            stage[x, y]++;
                            stage[x, y + 2]+= 2;
                            stage[x, y + 1] = stage[x, y];
                        }
                        else
                        {
                            stage[x, y + 1]++;
                            stage[x, y + 2]+= 2;
                            stage[x, y] = stage[x, y + 1];
                        }

                        if (stage[x, y + 2] > 0)
                            exceed = stage[x, y + 2];
					exceed_processing (exceed);
                        return 3;
                    }
                    //以上どれにも当てはまらない場合は、Lのまま落とす。
                    else
                    {
                        if (stage[x, y] >= stage[x, y + 1])
                        {
                            stage[x, y]+= 3;
                            stage[x, y + 1] = stage[x, y] - 2;
                        }
                        else
                        {
                            stage[x, y + 1]++;
                            stage[x, y] = stage[x, y + 1] + 2;
                        }
                        if (stage[x, y] > 0)
                            exceed = stage[x, y];
					exceed_processing (exceed);
                        return 0;
                    }
                }
                //block_dir == 0の場合(L)
                else
                {
                    //現地点が隣の地点より2以上高かった場合、Lを逆さまにした形で現地点に落とす。
                    if (x < width - 1 && stage[x, y] - stage[x + 1, y] >= 2)
                    {
                        stage[x, y]++;
                        stage[x + 1, y] = stage[x, y];
                        if (stage[x, y] > 0)
                            exceed = stage[x, y];
					exceed_processing (exceed);
                        return 2;
                    }

                    //現地点から隣の地点、更に隣の地点を参照し、一つ先の地点より現地点の高さが1小さく、二つ先の地点が一つ先の地点以下の場合にLの右側を下にして落とす。
                    else if (x < width - 2 && stage[x + 1, y] == stage[x + 2, y] + 1 && stage[x + 1, y] >= stage[x, y])
                    {
                        stage[x + 1, y]++;
                        stage[x, y] = stage[x + 1, y];
                        stage[x + 2, y] = stage[x, y];
                        if (stage[x, y] > 0)
                            exceed = stage[x, y];
					exceed_processing (exceed);
                        return 1;
                    }

                    //現地点が隣の地点とさらに隣の地点いずれかが同じ高さであった場合に,Lの左側を下にして落とす。
                    else if (x < width - 2 && (stage[x + 1, y] == stage[x, y] && stage[x + 2, y] <= stage[x, y] || stage[x + 2, y] == stage[x, y] && stage[x + 1, y] <= stage[x, y] || stage[x + 1, y] == stage[x + 2, y] && stage[x, y] <= stage[x + 1, y]))
                    {
                        if (stage[x, y] == stage[x + 1, y])
                        {
                            stage[x, y]++;
                            stage[x + 1, y]++;
                            stage[x + 2, y] = stage[x, y] + 1;
                        }
                        else if (stage[x, y] == stage[x + 2, y])
                        {
                            stage[x, y]++;
                            stage[x + 2, y]+= 2;
                            stage[x + 1, y] = stage[x, y];
                        }
                        else
                        {
                            stage[x + 1, y]++;
                            stage[x + 2, y]+= 2;
                            stage[x, y] = stage[x + 1, y];
                        }

                        if (stage[x + 2, y] > 0)
                            exceed = stage[x + 2, y];
					exceed_processing (exceed);
                        return 3;
                    }
                    //以上どれにも当てはまらない場合は、Lのまま落とす。
                    else
                    {
                        if (stage[x, y] >= stage[x + 1, y])
                        {
                            stage[x, y]+= 3;
                            stage[x + 1, y] = stage[x, y] - 2;
                        }
                        else
                        {
                            stage[x + 1, y]++;
                            stage[x, y] = stage[x + 1, y] + 2;
                        }
                        if (stage[x, y] > 0)
                            exceed = stage[x, y];
					exceed_processing (exceed);
                        return 0;
                    }
                }
            //s
            case 3:
                if(block_dir == 1)
                {
                    //現地点から一つ先、二つ先の地点を見て、一つ先より二つ先が1高く、現地点が一つ先以下の高さの場合sのまま落とす。
                    //また、採点時の条件に加えて現時点と一つ先が等しく、二つ先が現地点の高さ以下の場合も良しとする。
                    if (y < height - 2 && (stage[x, y + 2] == stage[x, y + 1] + 1 && stage[x, y + 1] >= stage[x, y] || stage[x, y + 1] == stage[x, y] && stage[x, y] >= stage[x, y + 2]))
                    {
                        stage[x, y + 1] += 2;
                        stage[x, y] = stage[x, y + 1] - 1;
                        stage[x, y + 2] = stage[x, y + 1];
                        if (stage[x, y + 1] > 0)
                            exceed = stage[x, y + 1];
					exceed_processing (exceed);
                        return 0;
                    }
                    //上の条件に当てはまらない場合は、sを90度回転させた形で落とす。
                    else
                    {
                        if(stage[x, y] > stage[x, y + 1])
                        {
                            stage[x, y] += 2;
                            stage[x, y + 1] = stage[x, y] - 1;
                        }
                        else
                        {
                            stage[x, y + 1] += 2;
                            stage[x, y] = stage[x, y + 1] + 1;
                        }
                        if (stage[x, y] > 0)
                            exceed = stage[x, y];
					exceed_processing (exceed);
                        return 1;
                    }

                }
                //block_dir == 0の場合(s)
                else
                {
                    //現地点から一つ先、二つ先の地点を見て、一つ先より二つ先が1高く、現地点が一つ先以下の高さの場合sのまま落とす。
                    //また、採点時の条件に加えて現時点と一つ先が等しく、二つ先が現地点の高さ以下の場合も良しとする。
                    if (x < width - 2 && (stage[x + 2, y] == stage[x + 1, y] + 1 && stage[x + 1, y] >= stage[x, y] || stage[x + 1, y] == stage[x, y] && stage[x, y] >= stage[x + 2, y]))
                    {
                        stage[x + 1, y] += 2;
                        stage[x, y] = stage[x + 1, y] - 1;
                        stage[x + 2, y] = stage[x + 1, y];
                        if (stage[x + 1, y] > 0)
                            exceed = stage[x + 1, y];
					exceed_processing (exceed);
                        return 0;
                    }
                    //上の条件に当てはまらない場合は、sを90度回転させた形で落とす。
                    else
                    {
                        if (stage[x, y] > stage[x + 1, y])
                        {
                            stage[x, y] += 2;
                            stage[x + 1, y] = stage[x, y] - 1;
                        }
                        else
                        {
                            stage[x + 1, y] += 2;
                            stage[x, y] = stage[x + 1, y] + 1;
                        }
                        if (stage[x, y] > 0)
                            exceed = stage[x, y];
					exceed_processing (exceed);
                        return 1;
                    }
                }
            //z
            case 4:
                if (block_dir == 1)
                {
                    //現地点から一つ先、二つ先の地点を見て、一つ先より現地点が1高く、二つ先が一つ先以下の高さの場合zのまま落とす。
                    //また、採点時の条件に加えて一つ先と二つ先が等しく、現地点が一つ先の高さ以下の場合も良しとする。
                    if (y < height - 2 && (stage[x, y] == stage[x, y + 1] + 1 && stage[x, y + 1] >= stage[x, y + 2] || stage[x, y + 1] == stage[x, y + 2] && stage[x, y + 1] >= stage[x, y]))
                    {
                        stage[x, y + 1] += 2;
                        stage[x, y + 2] = stage[x, y + 1] - 1;
                        stage[x, y] = stage[x, y + 1];
                        if (stage[x, y + 1] > 0)
                            exceed = stage[x, y + 1];
					exceed_processing (exceed);
                        return 0;
                    }
                    //上の条件に当てはまらない場合は、zを90度回転させた形で落とす。
                    else
                    {
                        if (stage[x, y + 1] > stage[x, y])
                        {
                            stage[x, y + 1] += 2;
                            stage[x, y] = stage[x, y + 1] - 1;
                        }
                        else
                        {
                            stage[x, y] += 2;
                            stage[x, y + 1] = stage[x, y] + 1;
                        }
                        if (stage[x, y + 1] > 0)
                            exceed = stage[x, y + 1];
					exceed_processing (exceed);
                        return 1;
                    }

                }
                //block_dir == 0の場合(z)
                else
                {
                    //現地点から一つ先、二つ先の地点を見て、一つ先より現地点が1高く、二つ先が一つ先以下の高さの場合sのまま落とす。
                    //また、採点時の条件に加えて一つ先と二つ先が等しく、現地点が一つ先の高さ以下の場合も良しとする。
                    if (x < width - 2 && (stage[x, y] == stage[x + 1, y] + 1 && stage[x + 1, y] >= stage[x + 2, y] || stage[x + 1, y] == stage[x + 2, y] && stage[x + 1, y] >= stage[x, y]))
                    {
					stage[x + 1, y] += 2;
					stage[x + 2, y] = stage[x + 1, y] - 1;
					stage[x, y] = stage[x + 1, y];
                        if (stage[x + 1, y] > 0)
                            exceed = stage[x + 1, y];
					exceed_processing (exceed);
                        return 0;
                    }
                    //上の条件に当てはまらない場合は、zを90度回転させた形で落とす。
                    else
                    {
                        if (stage[x + 1, y] > stage[x, y])
                        {
                            stage[x + 1, y] += 2;
                            stage[x, y] = stage[x + 1, y] - 1;
                        }
                        else
                        {
                            stage[x, y] += 2;
                            stage[x + 1, y] = stage[x, y] + 1;
                        }
                        if (stage[x + 1, y] > 0)
                            exceed = stage[x + 1, y];
					exceed_processing (exceed);
                        return 1;
                    }
                }
            //I
            case 5:
                //現地点より四つ先まで見て、三つ同じ高さ、残る一つはそれら以下の場合、Iを横にして落とす。

			if (block_dir == 1 && y < height - 3) {

				if (stage[x, y] == stage[x, y + 1]) {
					if (stage [x, y] == stage [x, y + 2]) {
						if (stage [x, y] == stage [x, y + 3])
							return 0;
						else if (stage [x, y] >= stage [x, y + 2])
							return 2;
					} else if (stage [x, y] == stage [x, y + 3] && stage [x, y] >= stage [x, y + 2])
						return 2;
				}
				else if (stage [x, y + 2] == stage [x, y + 3] && stage[x, y + 2] >= stage[x, y] && stage[x, y + 2] >= stage[x, y + 1] && (stage[x, y + 2] == stage[x, y] || stage[x, y + 2] == stage[x, y + 1])) {
					return 2;
				}
			}
			else if (block_dir == 0 && x < width - 3) {

				if (stage [x, y] == stage [x + 1, y]) {
					if (stage [x, y] == stage [x + 2, y]) {
						if (stage [x, y] == stage [x + 3, y])
							return 0;
						else if(stage[x, y] >= stage[x + 2, y])
							return 2;
					} else if (stage [x, y] == stage [x + 3, y] && stage [x, y] >= stage [x + 2, y])
						return 2;
				} else if (stage [x + 2, y] == stage [x + 3, y] && stage[x + 2, y] >= stage[x, y] && stage[x + 2, y] >= stage[x + 1, y] && (stage[x + 2, y] == stage[x, y] || stage[x + 2, y] == stage[x + 1, y])) {
					return 2;
				}
			}
			//ポイントは現地点の深さに4を足した値
			return 4 + stage[x, y];

			///以下旧プログラム
                if (block_dir == 1 && y < height - 3)
                {
                    if (stage[x, y] == stage[x, y + 1])
                    {
                        if (stage[x, y] == stage[x, y + 2])
                        {
                            stage[x, y]++;
                            stage[x, y + 1] = stage[x, y];
                            stage[x, y + 2] = stage[x, y];
                            stage[x, y + 3] = stage[x, y];
                            if (stage[x, y] > 0)
                                exceed = stage[x, y];
						exceed_processing (exceed);
                            return 1;
                        }
                        else if (stage[x, y] == stage[x, y + 3])
                        {
                            stage[x, y]++;
                            stage[x, y + 1] = stage[x, y];
                            stage[x, y + 2] = stage[x, y];
                            stage[x, y + 3] = stage[x, y];
                            if (stage[x, y] > 0)
                                exceed = stage[x, y];
						exceed_processing (exceed);
                            return 1;
                        }
                    }
                    else if (stage[x, y + 2] == stage[x, y + 3])
                    {
                        if (stage[x, y + 2] == stage[x, y + 1] || stage[x, y + 2] == stage[x, y])
                        {
                            stage[x, y + 1]++;
                            stage[x, y] = stage[x, y + 1];
                            stage[x, y + 2] = stage[x, y];
                            stage[x, y + 3] = stage[x, y];
                            if (stage[x, y] > 0)
                                exceed = stage[x, y];
						exceed_processing (exceed);
                            return 1;
                        }
                    }
                }
                else if (block_dir == 0 && x < width - 3)
                {
                    if (stage[x, y] == stage[x + 1, y])
                    {
					if (stage[x, y] == stage[x + 2, y] && stage[x, y] >= stage[x + 3, x])
                        {
                            stage[x, y]++;
                            stage[x + 1, y] = stage[x, y];
                            stage[x + 2, y] = stage[x, y];
                            stage[x + 3, y] = stage[x, y];
                            if (stage[x, y] > 0)
                                exceed = stage[x, y];
						exceed_processing (exceed);
                            return 1;
                        }
                    }
                    else if (stage[x + 2, y] == stage[x + 3, y])
                    {
                        if (stage[x + 2, y] == stage[x + 1, y] || stage[x + 2, y] == stage[x, y])
                        {
                            stage[x + 1, y]++;
                            stage[x, y] = stage[x + 1, y];
                            stage[x + 2, y] = stage[x, y];
                            stage[x + 3, y] = stage[x, y];
                            if (stage[x, y] > 0)
                                exceed = stage[x, y];
						exceed_processing (exceed);
                            return 1;
                        }
                    }
                }
                //Iのまま落とす。
                else
                {
                    stage[x, y] += 4;
                    if (stage[x, y] > 0)
                        exceed = stage[x, y];
				exceed_processing (exceed);
                    return 0;
                }
			return 0;
            default:
                return 0;
        }
    }

	//以上の関数を用いて点数計算をし、mapの更新をする。
	//DropBlock_Processingにブロックを落とす命令をする。
    public void drop(int blocknum, int block_dir)
    {
        /*
         * なるべく自機に近く、隙間なく収まる場所に落とす。
         */
		int player_x = (int)player.transform.position.x;
		int player_y = (int)player.transform.position.z;
        int x_pos = 0;
        int y_pos = 0;
		int drop_dir;

		//最適値を計算
		double high_score = drop_point (x_pos, y_pos, blocknum, block_dir) + Math.Abs((double)player_x - x_pos) + Math.Abs((double)player_y - y_pos) + stage[x_pos, y_pos];
		double point_now;

		for (int i = 0; i < width; i++) {
			for (int j = 0; j < height; j++) {
				point_now = drop_point (i, j, blocknum, block_dir) + Math.Abs((double)player_x - i) + Math.Abs((double)player_y - j) + stage[i, j];
				if (high_score > point_now) {
					x_pos = i;
					y_pos = j;
					high_score = point_now;
				}
			}
		}

		//mapの更新、drop_dirはブロックの回転方向
		drop_dir = drop_action (x_pos, y_pos, blocknum, block_dir);

		for (int i = 0; i < width; i++) {
			Debug.Log (stage[i,0].ToString() + ", " + stage[i,1].ToString() + ", " + stage[i,2].ToString() + ", " + stage[i,3].ToString() + ", " + stage[i,4].ToString() + ", " + stage[i,5].ToString() + ", " + stage[i,6].ToString() + ", ");
		}

		//dropBlockの第4の引数は座標(0, 0, 0)に一番近いブロックのy座標
		//点数計算は座標(0, 0, 0)に一番近いブロックの座標をx, yとして計算してきた為、
		//更に各ブロックは回転すると中心座標の位置が変わってしまう為
		switch (blocknum) {
		case 0:
			drop_pro.dropBlock (x_pos, y_pos, blocknum, block_dir, drop_dir, stage [x_pos, y_pos] - low_size - 2);
			break;
		case 1:
			if (drop_dir == 0 || drop_dir == 1 || drop_dir == 2)
				drop_pro.dropBlock(x_pos, y_pos, blocknum, block_dir, drop_dir, stage [x_pos, y_pos] - low_size - 1);
			else if(drop_dir == 0)
				drop_pro.dropBlock(x_pos, y_pos, blocknum, block_dir, drop_dir, stage [x_pos, y_pos] - low_size - 1);
			else
				drop_pro.dropBlock(x_pos, y_pos, blocknum, block_dir, drop_dir, stage [x_pos, y_pos] - low_size - 2);
			break;
		case 2:
			if (drop_dir == 2 || drop_dir == 3)
				drop_pro.dropBlock(x_pos, y_pos, blocknum, block_dir, drop_dir, stage [x_pos, y_pos] - low_size - 1);
			else if(drop_dir == 0)
				drop_pro.dropBlock(x_pos, y_pos, blocknum, block_dir, drop_dir, stage [x_pos, y_pos] - low_size - 3);
			else
				drop_pro.dropBlock(x_pos, y_pos, blocknum, block_dir, drop_dir, stage [x_pos, y_pos] - low_size - 2);
			break;
		case 3:
			if (drop_dir == 0)
				drop_pro.dropBlock(x_pos, y_pos, blocknum, block_dir, drop_dir, stage [x_pos, y_pos] - low_size - 1);
			else
				drop_pro.dropBlock(x_pos, y_pos, blocknum, block_dir, drop_dir, stage [x_pos, y_pos] - low_size - 3);
			break;
		case 4:
			if (drop_dir == 0)
				drop_pro.dropBlock(x_pos, y_pos, blocknum, block_dir, drop_dir, stage [x_pos, y_pos] - low_size - 2);
			else
				drop_pro.dropBlock(x_pos, y_pos, blocknum, block_dir, drop_dir, stage [x_pos, y_pos] - low_size - 2);
			break;
		case 5:
			if (drop_dir == 0)
				drop_pro.dropBlock(x_pos, y_pos, blocknum, block_dir, drop_dir, stage [x_pos, y_pos] - low_size - 4);
			else
				drop_pro.dropBlock(x_pos, y_pos, blocknum, block_dir, drop_dir, stage [x_pos, y_pos] - low_size - 1);
			break;
		default :
			drop_pro.dropBlock(x_pos, y_pos, blocknum, block_dir, drop_dir, 0);
			break;
		}

		if (lowsize_alert != 0)
			lowsize_alert_alert++;
    }

	//以下getterとreset
	public int get_map(int x, int y){
		return stage[x, y];
	}

	public int get_lowSize(){
		return low_size;
	}

	public int get_sizeAlert(){
		return lowsize_alert_alert;
	}

	public void sizeAlert_reset(){
		lowsize_alert_alert = 0;
		lowsize_alert = 0;
	}

    // Update is called once per frame
    void Update () {
	
	}
}
