﻿using UnityEngine;
using System;
using System.Collections.Generic;
using System.Linq;

public class FootPrintsManager : MonoBehaviour {
	private GameObject stand_block;
	private GameObject foot;
	public GameObject foot_block;
	public GameObject footPaintsIns;
	private int[] foot_way = new int[4];
	private int way_count = 0;
	private int way_figure;
    private int before_stand_dir = -1;
	//足跡のy座標の調整
	private const float footprints_pos = (float)(-0.45);

	private const int O_figure = 0;
	private const int J_figure = 1;
	private const int L_figure = 2;
	private const int S_figure = 3;
	private const int Z_figure = 4;
	private const int I_figure = 5;
	// Use this for initialization
	void Start(){
        //HECK : testCode
        DropBlock_Processing DropBlockProcessing = FindObjectOfType<DropBlock_Processing>();
        Dictionary<int, GameObject> blockDictionary = new Dictionary<int, GameObject>();
        blockDictionary.Add(O_figure, DropBlockProcessing.block_O);
        blockDictionary.Add(J_figure, DropBlockProcessing.block_J);
        blockDictionary.Add(L_figure, DropBlockProcessing.block_L);
        blockDictionary.Add(S_figure, DropBlockProcessing.block_S);
        blockDictionary.Add(Z_figure, DropBlockProcessing.block_Z);
        blockDictionary.Add(I_figure, DropBlockProcessing.block_I);

        Transform[] list = DropBlockProcessing.block_O.GetComponentsInChildren<Transform>();
        list = list.OrderBy(item => item.position.x).ThenBy(item => item.position.y).ToArray();
        Vector2[] vec = new Vector2[list.Length];
        for(int i = 0; i < list.Length; i++)
        {
            vec[i] = list[i].position;
        }
    }

	//リセット
	public void reset(){
		foreach (Transform n in footPaintsIns.transform) {
			GameObject.Destroy (n.gameObject);
		}
		way_count = 0;
	}

	/*
	 * Nowの足跡を保管、形状を判断してブロックを生成する。
	 * 第四引数は入力された方向で、これによりブロックの形状を判断する。
	 * 足跡が4つになった場合は全て消してブロック生成の処理を始める。
	 */
	public int put_footprints(float x, float y, float z, int stand_dir){
		foot = (GameObject)Instantiate (foot_block, new Vector3(x, y + footprints_pos, z), Quaternion.Euler(0, 0, 0));
		foot.transform.parent = footPaintsIns.transform;
		foot_way[way_count] = stand_dir;
        // 戻る時のみ座標が重なる
        if (before_stand_dir != stand_dir)
        {
            way_count++;
            before_stand_dir = stand_dir;
        }

		if (footPaintsIns.transform.childCount >= 4) {
            reset();

            /*
			 *配列foot_wayに入れてきた、stand_blockの進んだ道のりを確認する。
			 *ここで、0~1間はどのブロックでも同じなので確認しない。
			 *1~2が直進の時、3が2にとってleftならL, RightならJ, 直進ならIに絞れる。
			 *1~2が直進でない時、3が1に対して直進であればS,Z, そうでなければO, L, Jに絞れる。
			 */
            if (foot_way[1] == foot_way[2])
            {
                if (foot_way[2] == foot_way[3])
                {
                    way_figure = I_figure;
                }
                else if (foot_way[3] - foot_way[2] == 1 || foot_way[2] == 3 && foot_way[3] == 0)
                {
                    way_figure = J_figure;
                }
                else {
                    way_figure = L_figure;
                }
            }
            else if (Math.Abs(foot_way[3] - foot_way[1]) == 2)
            {
                way_figure = O_figure;
            }
            else if (foot_way[3] == foot_way[1])
            {
                if (foot_way[2] - foot_way[1] == 1 || foot_way[1] == 3 && foot_way[2] == 0)
                    way_figure = Z_figure;
                else
                    way_figure = S_figure;
            }
            else if (foot_way[2] - foot_way[1] == 1 || foot_way[1] == 3 && foot_way[2] == 0)
            {
                way_figure = L_figure;
            }
            else {
                way_figure = J_figure;
            }
			return (-way_figure);
		}
		return 1;
	}
	

}
