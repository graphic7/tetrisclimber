﻿using UnityEngine;
using System.Collections;

public class DropBlock_Processing : MonoBehaviour {
	public GameObject block_O;
	public GameObject block_J;
	public GameObject block_L;
	public GameObject block_S;
	public GameObject block_Z;
	public GameObject block_I;
	public GameObject block_ins;
	public int dis;
	private GameObject obj_ins;

	//リセット
	public void reset(){
		foreach (Transform n in block_ins.transform) {
			GameObject.Destroy (n.gameObject);
		}
	}

	//実際にブロックを落とす。Drop_Managerから呼ばれる。
	//引数は順にブロックの種類、ブロックの向き(x向きかz向きか)、ブロックの方向
	//drop_posはあくまでx, z座標の高さなので、各ブロックの中心のx, z座標と同じとは限らない
	//回転した場合の中心座標、ブロックの大きさと相談して座標を調整する。
	public void dropBlock(int x, int y, int blocknum, int block_dir, int drop_dir, int drop_pos){
		switch(blocknum){
		case 0:
			obj_ins = (GameObject)Instantiate (block_O, new Vector3((float)x, drop_pos, (float)y), Quaternion.Euler(0, (float)((-90) * block_dir), 0));
			break;
		case 1:
			if (drop_dir == 0) {
				obj_ins = (GameObject)Instantiate (block_J, new Vector3 ((float)x, drop_pos, (float)y), Quaternion.Euler (0, (float)((-90) * block_dir), 0));
			} 
			else if (drop_dir == 1) {
				if(block_dir == 0)
					obj_ins = (GameObject)Instantiate (block_J, new Vector3 ((float)x, drop_pos, (float)y), Quaternion.Euler (0, 0, 270));
				else
					obj_ins = (GameObject)Instantiate (block_J, new Vector3 ((float)x, drop_pos, (float)y), Quaternion.Euler (0, (float)(-90), 270));
			}
			else if (drop_dir == 2) {
				if(block_dir == 0)
					obj_ins = (GameObject)Instantiate (block_J, new Vector3 ((float)x + 1, drop_pos, (float)y), Quaternion.Euler (0, 0, 180));
				else
					obj_ins = (GameObject)Instantiate (block_J, new Vector3 ((float)x, drop_pos, (float)y + 1), Quaternion.Euler (0, (float)(-90), 180));
			}
			else if (drop_dir == 3) {
				if(block_dir == 0)
					obj_ins = (GameObject)Instantiate (block_J, new Vector3 ((float)x + 2, drop_pos, (float)y), Quaternion.Euler (0, 0, 90));
				else
					obj_ins = (GameObject)Instantiate (block_J, new Vector3 ((float)x, drop_pos, (float)y + 2), Quaternion.Euler (0, (float)(-90), 90));
			}
			break;
		case 2:
			if (drop_dir == 0) {
				obj_ins = (GameObject)Instantiate (block_L, new Vector3 ((float)x, drop_pos, (float)y), Quaternion.Euler (0, (float)((-90) * block_dir), 0));
			} 
			else if (drop_dir == 1) {
				if(block_dir == 0)
					obj_ins = (GameObject)Instantiate (block_L, new Vector3 ((float)x, drop_pos, (float)y), Quaternion.Euler (0, 0, 270));
				else
					obj_ins = (GameObject)Instantiate (block_L, new Vector3 ((float)x, drop_pos, (float)y), Quaternion.Euler (0, (float)(-90), 270));
			}
			else if (drop_dir == 2) {
				if(block_dir == 0)
					obj_ins = (GameObject)Instantiate (block_L, new Vector3 ((float)x + 1, drop_pos, (float)y), Quaternion.Euler (0, 0, 180));
				else
					obj_ins = (GameObject)Instantiate (block_L, new Vector3 ((float)x, drop_pos, (float)y + 1), Quaternion.Euler (0, (float)(-90), 180));
			}
			else if (drop_dir == 3) {
				if(block_dir == 0)
					obj_ins = (GameObject)Instantiate (block_L, new Vector3 ((float)x + 2, drop_pos, (float)y), Quaternion.Euler (0, 0, 90));
				else
					obj_ins = (GameObject)Instantiate (block_L, new Vector3 ((float)x, drop_pos, (float)y + 2), Quaternion.Euler (0, (float)(-90), 90));
			}
			break;
		case 3:
			if (drop_dir == 0) {
				obj_ins = (GameObject)Instantiate (block_S, new Vector3 ((float)x, drop_pos, (float)y), Quaternion.Euler (0, (float)((-90) * block_dir), 0));
			}
			else if (drop_dir == 1) {
				if(block_dir == 0)
					obj_ins = (GameObject)Instantiate (block_S, new Vector3 ((float)x + 1, drop_pos, (float)y), Quaternion.Euler (0, 0, 90));
				else
					obj_ins = (GameObject)Instantiate (block_S, new Vector3 ((float)x, drop_pos, (float)y + 1), Quaternion.Euler (0, (float)(-90), 90));
			}
			break;
		case 4:
			if (drop_dir == 0) {
				obj_ins = (GameObject)Instantiate (block_Z, new Vector3 ((float)x, drop_pos, (float)y), Quaternion.Euler (0, (float)((-90) * block_dir), 0));
			}
			else if (drop_dir == 1) {
				if(block_dir == 0)
					obj_ins = (GameObject)Instantiate (block_Z, new Vector3 ((float)x + 1, drop_pos, (float)y), Quaternion.Euler (0, 0, 90));
				else
					obj_ins = (GameObject)Instantiate (block_Z, new Vector3 ((float)x, drop_pos, (float)y + 1), Quaternion.Euler (0, (float)(-90), 90));
			}
			break;
		case 5:
			if (drop_dir == 0) {
				obj_ins = (GameObject)Instantiate (block_I, new Vector3 ((float)x, drop_pos, (float)y), Quaternion.Euler (0, 0, 0));
			}
			else if (drop_dir == 1) {
				if(block_dir == 0)
					obj_ins = (GameObject)Instantiate (block_I, new Vector3 ((float)x, drop_pos, (float)y), Quaternion.Euler (0, 0, (float)(-90)));
				else
					obj_ins = (GameObject)Instantiate (block_I, new Vector3 ((float)x, drop_pos, (float)y), Quaternion.Euler (0, (float)(-90), (float)(-90)));
			}
			break;
		default:
			break;
		}
		obj_ins.transform.parent = block_ins.transform;
	}

	void Start () {
		
	}

	// Update is called once per frame
	void Update () {
	
	}
}
