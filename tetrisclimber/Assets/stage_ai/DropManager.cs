﻿using UnityEngine;
using System.Collections;
using System;

public class DropManager : MonoBehaviour {
    private int[,] stage;
    private System.Random rnd;
    private const int width = 10;
    private const int height = 10;
    private int low_size;
    private int[] block;
    private int low_num;
    // Use this for initialization

    private int[] low_pos;
    private int[] low_point;
    private GameObject player;

    void Awake()
    {
        stage = new int[width, height];
        for (int i = 0; i < width; i++)
            for (int j = 0; j < height; j++)
                stage[i, j] = 0;
        low_num = 0;
        block = new int[7] { 0, 1, 2, 3, 4, 5, 6 };
        player = GameObject.Find("Now");
        rnd = new System.Random();
        low_size = 0;
    }

    void Start()
    {
    }

    //指定の場所にブロックが埋まるかどうかの点数計算
    //block_dirについて、J, Lの長い方がある側面に対して向いて落ちてくる。
    //例えば、Lのまま落ちてきたら、Lの長い方側にある、つまり左側から見て形が見えるように落ちてくる。
    //この場合block_dirは1、違う場合は0
    int drop_point(int x, int y, int blocknum, int block_dir)
    {
        int edge_x;
        int edge_y;
        if (x == 0)
        {
            if (y == 0)
            {
                edge_x = 1;
                edge_y = 1;
            }
            else if (y == height - 1)
            {
                edge_x = 1;
                edge_y = -1;
            }
            else
            {
                edge_x = 1;
                edge_y = 0;
            }
        }
        else if (x == width - 1)
        {
            if (y == 0)
            {
                edge_x = -1;
                edge_y = 1;
            }
            else if (y == height - 1)
            {
                edge_x = -1;
                edge_y = -1;
            }
            else
            {
                edge_x = -1;
                edge_y = 0;
            }
        }
        else if (y == 0)
        {
            edge_x = 0;
            edge_y = 1;
        }
        else if (y == height - 1)
        {
            edge_x = 0;
            edge_y = -1;
        }
        else
        {
            edge_x = 0;
            edge_y = 0;
        }

        switch (blocknum)
        {
            //o
            case 0:
                if (block_dir == 1 && (edge_y != 0 && stage[x, y + edge_y] == stage[x, y] || edge_y == 0 && stage[x, y + 1] == stage[x, y]) || block_dir == 0 && (edge_x != 0 && stage[x + edge_x, y] == stage[x, y] || edge_x == 0 && stage[x + 1, y] == stage[x, y]))
                    return 0;
                else
                    return 10;
            //J
            case 1:
                if (block_dir == 1 && (y < height - 1 && stage[x, y + 1] == stage[x, y] + 2 || edge_y != 0 && stage[x, y + edge_y] == stage[x, y] && stage[x, y + edge_y * 2] == stage[x, y] || y > 1 && stage[x, y - 1] == stage[x, y] + 1 && stage[x, y - 2] == stage[x, y] + 1 || edge_y == 0 && stage[x, y - 1] == stage[x, y] && stage[x, y + 1] == stage[x, y]) ||
                    block_dir == 0 && (x < width - 1 && stage[x + 1, y] == stage[x, y] + 2 || edge_x != 0 && stage[x + edge_x, y] == stage[x, y] && stage[x + edge_x * 2, y] == stage[x, y] || x > 1 && stage[x - 1, y] == stage[x, y] + 1 && stage[x - 2, y] == stage[x, y] + 1 || edge_x == 0 && stage[x - 1, y] == stage[x, y] && stage[x + 1, y] == stage[x, y]))
                    return 0;
                else if (block_dir == 1 && edge_y < height - 1 && stage[x, y + 1] > stage[x, y] + 2 || block_dir == 0 && x < width - 1 && stage[x + 1, y] > stage[x, y] + 2)
                    return 2;
                else if (block_dir == 1 && (edge_y != 0 && stage[x, y + edge_y] == stage[x, y] || edge_y == 0 && stage[x, y - 1] == stage[x, y]) || block_dir == 0 && (edge_x != 0 && stage[x + edge_x, y] == stage[x, y] || edge_x == 0 && stage[x - 1, y] == stage[x, y]))
                    return 5;
                else
                    return 10;
            //L
            case 2:
                if (block_dir == 1 && (y > 0 && stage[x, y - 1] == stage[x, y] + 2 ||edge_y != 0 && stage[x, y + edge_y] == stage[x, y] && stage[x, y + edge_y * 2] == stage[x, y] || y < height - 2 && stage[x, y + 1] == stage[x, y] + 1 && stage[x, y + 2] == stage[x, y] + 1 || edge_y == 0 && stage[x, y - 1] == stage[x, y] && stage[x, y + 1] == stage[x, y]) || 
                    block_dir == 0 && (x > 0 && stage[x - 1, y] == stage[x, y] + 2 || edge_x != 0 && stage[x + edge_x, y] == stage[x, y] && stage[x + edge_x * 2, y] == stage[x, y] || x < width - 2 && stage[x + 1, y] == stage[x, y] + 1 && stage[x + 2, y] == stage[x, y] + 1 || edge_x == 0 && stage[x - 1, y] == stage[x, y] && stage[x + 1, y] == stage[x, y]))
                    return 0;
                else if (block_dir == 1 && edge_y > 0 && stage[x, y - 1] > stage[x, y] + 2 || block_dir == 0 && x > 0 && stage[x - 1, y] > stage[x, y] + 2)
                    return 2;
                else if (block_dir == 1 && (edge_y != 0 && stage[x, y + edge_y] == stage[x, y] || edge_y == 0 && stage[x, y + 1] == stage[x, y]) || block_dir == 0 && (edge_x != 0 && stage[x + edge_x, y] == stage[x, y] || edge_x == 0 && stage[x + 1, y] == stage[x, y]))
                    return 5;
                else
                    return 10;
            //s
            case 3:
                if (block_dir == 1 && y < height - 2 && stage[x, y + 1] == stage[x, y] && stage[x, y + 2] == stage[x, y] + 1 || block_dir == 0 && x < width - 2 && stage[x + 1, y] == stage[x, y] && stage[x + 2, y] == stage[x, y] + 1)
                    return 0;
                else if (block_dir == 1 && edge_y != 1 && stage[x, y - 1] == stage[x, y] + 1 || block_dir == 0 && edge_x != 1 && stage[x - 1, y] == stage[x, y])
                    return 2;
                else
                    return 10;
            //z
            case 4:
                if (block_dir == 1 && y > 2 && stage[x, y - 1] == stage[x, y] && stage[x, y - 2] == stage[x, y] + 1 || block_dir == 0 && x > 2 && stage[x - 1, y] == stage[x, y] && stage[x - 2, y] == stage[x, y] + 1)
                    return 0;
                else if (block_dir == 1 && edge_y != -1 && stage[x, y + 1] == stage[x, y] + 1 || block_dir == 0 && edge_x != -1 && stage[x + 1, y] == stage[x, y] + 1)
                    return 2;
                else
                    return 10;
            //I
            case 5:
                if (block_dir == 1 && y < height - 3 && stage[x, y + edge_y] == stage[x, y] && stage[x, y + edge_y * 2] == stage[x, y] && stage[x, y + edge_y * 3] == stage[x, y] || block_dir == 0 && x < width - 3 && stage[x + edge_x * 1, y] == stage[x, y] && stage[x + edge_x * 2, y] == stage[x, y] && stage[x + edge_x * 3, y] == stage[x, y])
                    return 0;
                else
                    return 3;
            default:
                return 10;
        }
    }

    void drop_action(int x, int y, int blocknum, int block_dir)
    {

        int exceed = 0;
        int edge_x;
        int edge_y;
        if (x == 0)
        {
            if (y == 0)
            {
                edge_x = 1;
                edge_y = 1;
            }
            else if (y == height - 1)
            {
                edge_x = 1;
                edge_y = -1;
            }
            else
            {
                edge_x = 1;
                edge_y = 0;
            }
        }
        else if (x == width - 1)
        {
            if (y == 0)
            {
                edge_x = -1;
                edge_y = 1;
            }
            else if (y == height - 1)
            {
                edge_x = -1;
                edge_y = -1;
            }
            else
            {
                edge_x = -1;
                edge_y = 0;
            }
        }
        else if (y == 0)
        {
            edge_x = 0;
            edge_y = 1;
        }
        else if (y == height - 1)
        {
            edge_x = 0;
            edge_y = -1;
        }
        else
        {
            edge_x = 0;
            edge_y = 0;
        }
        switch (blocknum)
        {
            //o
            case 0:
                if(block_dir == 1)
                {
                    
                    if(edge_y != -1)
                    {
                        if (stage[x, y] > stage[x, y + 1])
                            stage[x, y + 1] = stage[x, y];
                        else if (stage[x, y] < stage[x, y + 1])
                            stage[x, y] = stage[x, y + 1];

                        stage[x, y] += 2;
                        stage[x, y + 1] += 2;
                    }
                    else
                    {
                        if (stage[x, y] > stage[x, y - 1])
                            stage[x, y - 1] = stage[x, y];
                        else if (stage[x, y] < stage[x, y - 1])
                            stage[x, y] = stage[x, y - 1];

                        stage[x, y] += 2;
                        stage[x, y - 1] += 2;
                    }

                }
                else
                {

                    if (edge_x != -1)
                    {
                        if (stage[x, y] > stage[x + 1, y])
                            stage[x + 1, y] = stage[x, y];
                        else if (stage[x, y] < stage[x + 1, y])
                            stage[x, y] = stage[x + 1, y];

                        stage[x, y] += 2;
                        stage[x + 1, y] += 2;
                    }
                    else
                    {
                        if (stage[x, y] > stage[x - 1, y])
                            stage[x - 1, y] = stage[x, y];
                        else if (stage[x, y] < stage[x - 1, y])
                            stage[x, y] = stage[x - 1, y];

                        stage[x, y] += 2;
                        stage[x - 1, y] += 2;
                    }

                }
                if (stage[x, y] > 0)
                        exceed = stage[x, y];
                break;
            //J
            case 1:
                if (block_dir == 1)
                {
                    if(y < height - 1 && stage[x, y + 1] >= stage[x, y] + 2)
                    {
                        stage[x, y + 1] += 1;
                        stage[x, y] = stage[x, y + 1];
                        if (stage[x, y] > 0)
                            exceed = stage[x, y];
                    }
                    else if(edge_y != 0 && stage[x, y + edge_y] == stage[x, y] && stage[x, y + edge_y * 2] == stage[x, y])
                    {
                        if(edge_y == -1)
                        {
                            stage[x, y] += 1;
                            stage[x, y - 1] += 1;
                            stage[x, y - 2] += 2;
                            if (stage[x, y - 2] > 0)
                                exceed = stage[x, y - 2];
                        }
                        else
                        {
                            stage[x, y] += 2;
                            stage[x, y + 1] += 1;
                            stage[x, y + 2] += 1;
                            if (stage[x, y] > 0)
                                exceed = stage[x, y];
                        }
                    }
                    else if(y > 1 && stage[x, y - 1] == stage[x, y] + 1 && stage[x, y - 2] == stage[x, y] + 1)
                    {
                        stage[x, y] += 1;
                        stage[x, y - 1] += 1;
                        stage[x, y - 2] += 2;
                        if (stage[x, y - 2] > 0)
                            exceed = stage[x, y - 2];
                    }
                    else if(y == 1 && stage[x, y - 1] == stage[x, y] && stage[x, y + 1] == stage[x, y])
                    {
                        stage[x, y] += 1;
                        stage[x, y + 1] += 1;
                        stage[x, y - 1] += 2;
                        if (stage[x, y - 1] > 0)
                            exceed = stage[x, y - 1];
                    }
                    else
                    {

                        if (y > 0)
                        {
                            if (stage[x, y] > stage[x, y - 1])
                                stage[x, y - 1] = stage[x, y];
                            else if (stage[x, y] < stage[x, y - 1])
                                stage[x, y] = stage[x, y - 1];

                            stage[x, y - 1] += 1;
                            stage[x, y] += 2;

                            if (stage[x, y] > 0)
                                exceed = stage[x, y];
                        }
                        else
                        {
                            if (stage[x, y] > stage[x, y + 1])
                                stage[x, y + 1] = stage[x, y];
                            else if (stage[x, y] < stage[x, y + 1])
                                stage[x, y] = stage[x, y + 1];

                            stage[x, y + 1] += 2;
                            stage[x, y] += 1;
                            if (stage[x, y + 1] > 0)
                                exceed = stage[x, y + 1];
                        }

                    }
                    
                }

                if (block_dir == 0)
                {
                    if (x < width - 1 && stage[x + 1, y] >= stage[x, y] + 2)
                    {
                        stage[x + 1, y] += 1;
                        stage[x, y] = stage[x + 1, y];
                        if (stage[x, y] > 0)
                            exceed = stage[x, y];
                    }
                    else if (edge_x != 0 && stage[x + edge_x, y] == stage[x, y] && stage[x + edge_x * 2, y] == stage[x, y])
                    {
                        if (edge_x == -1)
                        {
                            stage[x, y] += 1;
                            stage[x - 1, y] += 1;
                            stage[x - 2, y] += 2;
                            if (stage[x - 2, y] > 0)
                                exceed = stage[x - 2, y];
                        }
                        else
                        {
                            stage[x, y] += 2;
                            stage[x + 1, y] += 1;
                            stage[x + 2, y] += 1;
                            if (stage[x + 2, y] > 0)
                                exceed = stage[x + 2, y];
                        }
                    }
                    else if (x > 1 && stage[x - 1, y] == stage[x, y] + 1 && stage[x - 2, y] == stage[x, y] + 1)
                    {
                        stage[x, y] += 1;
                        stage[x - 1, y] += 1;
                        stage[x - 2, y] += 2;
                        if (stage[x - 2, y] > 0)
                            exceed = stage[x - 2, y];
                    }
                    else if (x == 1 && stage[x - 1, y] == stage[x, y] && stage[x + 1, y] == stage[x, y])
                    {
                        stage[x, y] += 1;
                        stage[x + 1, y] += 1;
                        stage[x - 1, y] += 2;
                        if (stage[x - 1, y] > 0)
                            exceed = stage[x - 1, y];
                    }
                    else
                    {

                        if (x > 0)
                        {
                            if (stage[x, y] > stage[x - 1, y])
                                stage[x - 1, y] = stage[x, y];
                            else if (stage[x, y] < stage[x - 1, y])
                                stage[x, y] = stage[x - 1, y];

                            stage[x - 1, y] += 1;
                            stage[x, y] += 2;
                            if (stage[x, y] > 0)
                                exceed = stage[x, y];
                        }
                        else
                        {
                            if (stage[x, y] > stage[x + 1, y])
                                stage[x + 1, y] = stage[x, y];
                            else if (stage[x, y] < stage[x + 1, y])
                                stage[x, y] = stage[x + 1, y];

                            stage[x + 1, y] += 2;
                            stage[x, y] += 1;
                            if (stage[x + 1, y] > 0)
                                exceed = stage[x + 1, y];
                        }

                    }
                }
                break;
            //L
            case 2:
                if (block_dir == 1)
                {
                    if (y > 0 && stage[x, y - 1] >= stage[x, y] + 2)
                    {
                        stage[x, y - 1] += 1;
                        stage[x, y] = stage[x, y - 1];
                        if (stage[x, y] > 0)
                            exceed = stage[x, y];
                    }
                    else if (edge_y != 0 && stage[x, y + edge_y] == stage[x, y] && stage[x, y + edge_y * 2] == stage[x, y])
                    {
                        if (edge_y == -1)
                        {
                            stage[x, y] += 2;
                            stage[x, y - 1] += 1;
                            stage[x, y - 2] += 1;
                            if (stage[x, y] > 0)
                                exceed = stage[x, y];
                        }
                        else
                        {
                            stage[x, y] += 1;
                            stage[x, y + 1] += 1;
                            stage[x, y + 2] += 2;
                            if (stage[x, y + 2] > 0)
                                exceed = stage[x, y + 2];
                        }
                    }
                    else if (y < height - 2 && stage[x, y + 1] == stage[x, y] + 1 && stage[x, y + 2] == stage[x, y] + 1)
                    {
                        stage[x, y] += 2;
                        stage[x, y - 1] += 1;
                        stage[x, y - 2] += 1;
                        if (stage[x, y] > 0)
                            exceed = stage[x, y];
                    }
                    else if (y == 1 && stage[x, y - 1] == stage[x, y] && stage[x, y + 1] == stage[x, y])
                    {
                        stage[x, y] += 1;
                        stage[x, y + 1] += 1;
                        stage[x, y - 1] += 2;
                        if (stage[x, y - 1] > 0)
                            exceed = stage[x, y - 1];
                    }
                    else
                    {

                        if (y > 0)
                        {
                            if (stage[x, y] > stage[x, y - 1])
                                stage[x, y - 1] = stage[x, y];
                            else if (stage[x, y] < stage[x, y - 1])
                                stage[x, y] = stage[x, y - 1];

                            stage[x, y - 1] += 1;
                            stage[x, y] += 2;
                            if (stage[x, y] > 0)
                                exceed = stage[x, y];
                        }
                        else
                        {
                            if (stage[x, y] > stage[x, y + 1])
                                stage[x, y + 1] = stage[x, y];
                            else if (stage[x, y] < stage[x, y + 1])
                                stage[x, y] = stage[x, y + 1];

                            stage[x, y + 1] += 2;
                            stage[x, y] += 1;
                            if (stage[x, y + 1] > 0)
                                exceed = stage[x, y + 1];
                        }

                    }

                }

                if (block_dir == 0)
                {
                    if (x < width - 1 && stage[x + 1, y] >= stage[x, y] + 2)
                    {
                        stage[x + 1, y] += 1;
                        stage[x, y] = stage[x + 1, y];
                        if (stage[x, y] > 0)
                            exceed = stage[x, y];
                    }
                    else if (edge_x != 0 && stage[x + edge_x, y] == stage[x, y] && stage[x + edge_x * 2, y] == stage[x, y])
                    {
                        if (edge_x == -1)
                        {
                            stage[x, y] += 2;
                            stage[x - 1, y] += 1;
                            stage[x - 2, y] += 1;
                            if (stage[x, y] > 0)
                                exceed = stage[x, y];
                        }
                        else
                        {
                            stage[x, y] += 1;
                            stage[x + 1, y] += 1;
                            stage[x + 2, y] += 2;
                            if (stage[x + 2, y] > 0)
                                exceed = stage[x + 2, y];
                        }
                    }
                    else if (x > 1 && stage[x - 1, y] == stage[x, y] + 1 && stage[x - 2, y] == stage[x, y] + 1)
                    {
                        stage[x, y] += 1;
                        stage[x - 1, y] += 1;
                        stage[x - 2, y] += 2;
                        if (stage[x - 2, y] > 0)
                            exceed = stage[x - 2, y];
                    }
                    else if (x == 1 && stage[x - 1, y] == stage[x, y] && stage[x + 1, y] == stage[x, y])
                    {
                        stage[x, y] += 1;
                        stage[x + 1, y] += 1;
                        stage[x - 1, y] += 2;
                        if (stage[x - 1, y] > 0)
                            exceed = stage[x - 1, y];
                    }
                    else
                    {

                        if (x > 0)
                        {
                            if (stage[x, y] > stage[x - 1, y])
                                stage[x - 1, y] = stage[x, y];
                            else if (stage[x, y] < stage[x - 1, y])
                                stage[x, y] = stage[x - 1, y];

                            stage[x - 1, y] += 1;
                            stage[x, y] += 2;
                            if (stage[x, y] > 0)
                                exceed = stage[x, y];
                        }
                        else
                        {
                            if (stage[x, y] > stage[x + 1, y])
                                stage[x + 1, y] = stage[x, y];
                            else if (stage[x, y] < stage[x + 1, y])
                                stage[x, y] = stage[x + 1, y];

                            stage[x + 1, y] += 2;
                            stage[x, y] += 1;
                            if (stage[x + 1, y] > 0)
                                exceed = stage[x + 1, y];
                        }

                    }
                }
                break;
            //s
            case 3:
                if(block_dir == 1)
                {
                    if(y < height - 2 && stage[x, y + 1] == stage[x, y] && stage[x, y + 2] == stage[x, y] + 1)
                    {
                        stage[x, y] += 1;
                        stage[x, y + 1] += 2;
                        stage[x, y + 2] += 1;
                        if (stage[x, y + 1] > 0)
                            exceed = stage[x, y + 1];
                    }
                    else
                    {
                        if(y > 0)
                        {
                            if (stage[x, y - 1] > stage[x, y] + 1)
                                stage[x, y] = stage[x, y - 1] - 1;
                            else if(stage[x, y + 1] < stage[x, y] + 1)
                                stage[x, y + 1] = stage[x, y] + 1;
                            stage[x, y] += 2;
                            stage[x, y - 1] += 2;
                            if (stage[x, y] > 0)
                                exceed = stage[x, y];
                        }
                        else
                        {
                            int high_size = stage[x, y];
                            if (high_size < stage[x, y - 1])
                                high_size = stage[x, y - 1];
                            if (high_size < stage[x, y - 2])
                                high_size = stage[x, y - 2];

                            if(high_size == stage[x, y])
                            {
                                stage[x, y] += 2;
                                stage[x, y - 1] += 2;
                                stage[x, y - 1] += 1;
                                if (stage[x, y] > 0)
                                    exceed = stage[x, y];
                            }
                            else{
                                stage[x, y] = 2 + high_size;
                                stage[x, y - 1] = 2 + high_size;
                                stage[x, y - 1] = 1 + high_size;
                                if (stage[x, y] > 0)
                                    exceed = stage[x, y];
                            }

                        }
                    }
                }
                if (block_dir == 0)
                {
                    if (x < width - 2 && stage[x + 1, y] == stage[x, y] && stage[x + 2, y] == stage[x, y] + 1)
                    {
                        stage[x, y] += 1;
                        stage[x + 1, y] += 2;
                        stage[x + 2, y] += 1;
                        if (stage[x + 1, y] > 0)
                            exceed = stage[x + 1, y];
                    }
                    else
                    {
                        if (x > 0)
                        {
                            if (stage[x - 1, y] > stage[x, y] + 1)
                                stage[x, y] = stage[x + 1, y] - 1;
                            else if (stage[x + 1, y] < stage[x, y] - 1)
                                stage[x + 1, y] = stage[x, y] + 1;
                            stage[x, y] += 2;
                            stage[x + 1, y] += 2;
                            if (stage[x + 1, y] > 0)
                                exceed = stage[x + 1, y];
                        }
                        else
                        {
                            int high_size = stage[x, y];
                            if (high_size < stage[x - 1, y])
                                high_size = stage[x - 1, y];
                            if (high_size < stage[x - 2, y])
                                high_size = stage[x - 2, y];

                            if (high_size == stage[x, y])
                            {
                                stage[x, y] += 2;
                                stage[x - 1, y] += 2;
                                stage[x - 2, y] += 1;
                                if (stage[x, y] > 0)
                                    exceed = stage[x, y];
                            }
                            else
                            {
                                stage[x, y] = 2 + high_size;
                                stage[x - 1, y] = 2 + high_size;
                                stage[x - 2, y] = 1 + high_size;
                                if (stage[x, y] > 0)
                                    exceed = stage[x, y];
                            }

                        }
                    }
                }
                break;
            //z
            case 4:
                if (block_dir == 1)
                {
                    if (y > 1 && stage[x, y - 1] == stage[x, y] && stage[x, y - 2] == stage[x, y] + 1)
                    {
                        stage[x, y] += 1;
                        stage[x, y - 1] += 2;
                        stage[x, y - 2] += 1;
                    }
                    else
                    {
                        if (y > 0)
                        {
                            if (stage[x, y - 1] > stage[x, y] + 1)
                                stage[x, y] = stage[x, y - 1] - 1;
                            else if (stage[x, y - 1] < stage[x, y] - 1)
                                stage[x, y - 1] = stage[x, y] + 1;
                            stage[x, y] += 2;
                            stage[x, y - 1] += 2;
                        }
                        else
                        {
                            int high_size = stage[x, y];
                            if (high_size < stage[x, y + 1])
                                high_size = stage[x, y + 1];
                            if (high_size < stage[x, y + 2])
                                high_size = stage[x, y + 2];

                            if (high_size == stage[x, y])
                            {
                                stage[x, y] += 2;
                                stage[x, y + 1] += 2;
                                stage[x, y + 2] += 1;
                            }
                            else
                            {
                                stage[x, y] = 2 + high_size;
                                stage[x, y + 1] = 2 + high_size;
                                stage[x, y + 2] = 1 + high_size;
                            }

                        }
                    }
                }
                if (block_dir == 0)
                {
                    if (x > 1 && stage[x - 1, y] == stage[x, y] && stage[x - 2, y] == stage[x, y] + 1)
                    {
                        stage[x, y] += 1;
                        stage[x - 1, y] += 2;
                        stage[x - 2, y] += 1;
                    }
                    else
                    {
                        if (x >0)
                        {
                            if (stage[x - 1, y] > stage[x, y] + 1)
                                stage[x, y] = stage[x - 1, y] - 1;
                            else if (stage[x - 1, y] < stage[x, y] - 1)
                                stage[x - 1, y] = stage[x, y] + 1;
                            stage[x, y] += 2;
                            stage[x - 1, y] += 2;
                        }
                        else
                        {
                            int high_size = stage[x, y];
                            if (high_size < stage[x + 1, y])
                                high_size = stage[x + 1, y];
                            if (high_size < stage[x + 2, y])
                                high_size = stage[x + 2, y];

                            if (high_size == stage[x, y])
                            {
                                stage[x, y] += 2;
                                stage[x + 1, y] += 2;
                                stage[x + 2, y] += 1;
                            }
                            else
                            {
                                stage[x, y] = 2 + high_size;
                                stage[x + 1, y] = 2 + high_size;
                                stage[x + 2, y] = 1 + high_size;
                            }

                        }
                    }
                }
                break;
            //I
            case 5:
                if (block_dir == 1)
                {
                    if(y < height - 3 && stage[x, y + 1] == stage[x, y] && stage[x, y + 2] == stage[x, y] && stage[x, y + 3] == stage[x, y])
                    {
                        stage[x, y] += 1;
                        stage[x, y + 1] += 1;
                        stage[x, y + 2] += 1;
                        stage[x, y + 3] += 1;
                    }
                    else
                        stage[x, y] += 4;
                }
                if (block_dir == 0)
                {
                    if (x < width - 3 && stage[x + 1, y] == stage[x, y] && stage[x + 2, y] == stage[x, y] && stage[x + 3, y] == stage[x, y])
                    {
                        stage[x, y] += 1;
                        stage[x + 1, y] += 1;
                        stage[x + 2, y] += 1;
                        stage[x + 3, y] += 1;
                    }
                    else
                    {
                        stage[x, y] += 4;
                    }
                }
                break;
            default:
                break;
        }
        low_size = stage[0, 0] - exceed;
        low_num = 1;
            for (int i = 0; i < width; i++)
                for (int j = 0; j < height; j++)
                {
                    stage[i, j] -= exceed;
                if (low_size > stage[i, j])
                {
                    low_size = stage[i, j];
                    low_num = 1;
                }
                else if (low_size == stage[i, j])
                    low_num++;
                }
        
    }

    public void drop(int blocknum, int block_dir)
    {
        /*
         * 最低値が0の場合はランダムで落とす
         * 最低値数が半分以上であればランダムで落とす
         * そうでない場合は、全ての最低値を考慮し、なるべく自機に近く、隙間なく収まる場所に落とす。
         */
        int x_pos;
        int y_pos;
        //最低値が0または半数以上の場合
            if (low_num == 0 || low_num >= width * height / 2)
            {
                int drop_pos = rnd.Next(low_num);
                x_pos = drop_pos / 10;
                y_pos = drop_pos % 10;
            }
            else
            {
            //全ての最低値を格納するlow_pos、ポイントをカウントするlow_point(ポイントは低い方が良い)
                low_pos = new int[low_num];
                low_point = new int[low_num];
            
                int low_count = 0;
                for (int i = 0; i < width; i++)
                {
                    for (int j = 0; j < height; j++)
                    {
                        if (stage[i, j] == low_size)
                        {
                            low_pos[low_count] = i * 10 + j;
                            low_count++;
                        }
                    if (low_count == low_num)
                        break;
                    }
                }
            int player_x = (int)player.transform.position.x;
            int player_y = (int)player.transform.position.z;
            int low_x;
            int low_y;
            int high_score = 0;

            for (int k = 0; k < low_num; k++)
                {
                low_x = low_pos[k] / 10;
                low_y = low_pos[k] % 10;

                low_point[k] = Math.Abs(low_x - player_x + low_y - player_y) + drop_point(low_x, low_y, blocknum, block_dir);
                if (high_score == k || low_point[high_score] > low_point[k])
                    high_score = k;
                }
            x_pos = low_pos[high_score] / 10;
            y_pos = low_pos[high_score] % 10;
            drop_action(x_pos, y_pos, blocknum, block_dir);
        }
            
    }
	
	// Update is called once per frame
	void Update () {
	
	}
}
