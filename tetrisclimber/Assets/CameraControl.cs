﻿using UnityEngine;
using System.Collections;
using Leap;

public class CameraControl: MonoBehaviour {

    public float mouseSensitivity = 8f;
    public float mouseScrollSensitivity = 2f;
    public float upDownRange = 90;
    public float distanceRange = 20f;
    private SmoothFollow smooth;
    private float cameraDistance;

    // Use this for initialization
    void Start () {
        smooth = Camera.main.GetComponent<SmoothFollow>();
        cameraDistance = smooth.distance;
    }
	
	// Update is called once per frame
	void Update () {
        smooth.rotation += Input.GetAxis("Mouse X") * mouseSensitivity;

        cameraDistance -= Input.GetAxis("Mouse ScrollWheel") * mouseScrollSensitivity;
        cameraDistance = Mathf.Clamp(cameraDistance, 0, distanceRange);
        smooth.distance = cameraDistance;
    }
}
