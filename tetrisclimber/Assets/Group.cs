﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

/// <summary>
/// コード制作者：キム ホンフン
/// このClassはブロックを移動するためにある。
/// 目的の場所まで動く経路はBlackNavigatorに委任している。
/// BlackNavigatorに従って動く。
/// </summary>

public class Group : MonoBehaviour
{
    // reference : http://noobtuts.com/unity/2d-tetris-game

    private Vector3 destinationLocation;
    private float lastBlockMoveTime;
    private float lastPutBlockTime;
    private float moveDelay = 0.5f;
    private float speed = 1.0f;
    public bool isGameOver { get; private set; }
    // Up, Downはz軸を動く
    public enum move { Left, Right, Forward, Back, Space, RotateZ, RotateX, Down, None };
    private Queue<Group.move> RouteQueue = new Queue<Group.move>();

    // Use this for initialization
    void Start()
    {
        this.destinationLocation = transform.position;
        transform.position = FindObjectOfType<Spawner>().transform.position;
        RouteReSearch(transform.position);
    }

    // Update is called once per frame
    void Update()
    {
        move move = move.None;
        if (RouteQueue.Count != 0 && Time.time - lastBlockMoveTime >= moveDelay)
        {
            move = RouteQueue.Dequeue();
            lastBlockMoveTime = Time.time;
        }

        if (Time.time - lastPutBlockTime >= 1.0f / speed)
        {
            move = move.Down;
            RouteReSearch(transform.position);
        }
        switch (move)
        {
            case move.Left:
                MoveBlock(Vector3.left);
                break;
            case move.Right:
                MoveBlock(Vector3.right);
                break;
            case move.Forward:
                MoveBlock(Vector3.forward);
                break;
            case move.Back:
                MoveBlock(Vector3.back);
                break;
            case move.Down:
                if (!MoveBlock(Vector3.down))
                {
                    enabled = false;

                    Vector3 _destinationLocation = destinationLocation;
                    Vector3 _nowPos = transform.position;
                    Debug.Assert(_destinationLocation.ToString().Equals(_nowPos.ToString()), "Destination Not Match Error" + "goal : " + _destinationLocation + " now : " + _nowPos);
                }

                lastPutBlockTime = Time.time;
                break;
            case move.Space:
                Debug.Assert(false, "Invalid tetrisblock move");
                //while (MoveBlock(Vector3.down)) ;

                enabled = false;
                break;
            case move.RotateZ:
                RotateBlock(0, 0, 90);
                break;
            case move.RotateX:
                RotateBlock(90, 0, 0);
                break;
            default:
                break;
        }
    }

    private bool MoveBlock(Vector3 vector3)
    {
        transform.position += vector3;

        if (IsValidPosition(transform.position))
        {
            return true;
        }
        else
        {
            transform.position -= vector3;
            //Debug.Assert(false, "BlockNavigatorが指定した動きには動けない！");
            return false;
        }
    }

    private bool RotateBlock(int x, int y, int z)
    {
        transform.Rotate(x, y, z);

        if (IsValidPosition(transform.position))
        {
            return true;
        }
        else
        {
            transform.Rotate(x, y, -z);
            return false;
        }
    }

    // 注意 : この関数は自分自身のブロックと重なる場合は考えていません。
    //        Drop_Managerにはその情報を持っていません。
    private bool IsValidPosition(Vector3 position)
    {
        int[] _position = RoundVec3(position);

        if (_position[1] < (int)destinationLocation.y)
        {
            return false;
        }

        //int z = DropManager.get_map(_position[1], (int)position.y);
        int z = 0;

        if (_position[1] - z > -2)
        {
            return true;
            //return false;
        }
        else
        {
            return true;
        }
    }

    internal void RouteReSearch(Vector3 nowPosition)
    {
        RouteQueue.Clear();

        // x 경로로 움직이고 y로 움직임.
        // TODO : 나중에 Dijkstra's algorithm을 활용하여 3차원 길찾기를 해야됨.
        // 道探しの法則：xの経路を動いてyの座標に合うように動く。
        // TODO : もし、障害物を避ける動作が必要ならば最短道探しアルゴリズムを適用するべき。(Dijkstra's algorithmなど)
        int[] _position = RoundVec3(nowPosition);

        int diff_x = _position[0] - (int)destinationLocation.x;
        move temp = move.None;

        if (diff_x > 0)
        {
            temp = move.Left;
        }
        else
        {
            temp = move.Right;
        }

        RouteEnqueue(temp, diff_x);

        int diff_y = _position[2] - (int)destinationLocation.z;

        if (diff_y < 0)
        {
            temp = move.Forward;
        }
        else
        {
            temp = move.Back;
        }

        RouteEnqueue(temp, diff_y);
    }

    private void RouteEnqueue(move move, int cnt)
    {
        for (int i = 0; i < Math.Abs(cnt); i++)
        {
            RouteQueue.Enqueue(move);
        }
    }

    protected int[] RoundVec3(Vector3 vector)
    {
        int[] points = new int[3];
        vector.x = points[0] = Mathf.RoundToInt(vector.x);
        vector.y = points[1] = Mathf.RoundToInt(vector.y);
        vector.z = points[2] = Mathf.RoundToInt(vector.z);
        return points;
    }
}